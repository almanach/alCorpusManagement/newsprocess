#!/usr/bin/perl

## To get some stats on a set of results files

use Data::Dumper;
use strict;
use Encode;
use YAML::Syck qw/DumpFile Dump/;

my @files = @ARGV || glob('*/*.xml') || glob('*.xml');

my $info = {
	    files => 0,
	    citations => { nb => 0,
			   agent => { nb => 0,
				      entity => 0,
				      indirect_entity => 0,
				      dist => {}
				    },
			   thema => 0,
			   audience => 0,
			   sxpipe => 0,
			   pred => {},
			   date => 0,
			   neg => 0,
			 },
	    verbatim => 0,
	    entities => { nb => 0,
			  occ => 0,
			  dist => {},
			  type => {},
			  anaphora => 0,
			},
	    sentences => { nb => 0,
			   full =>0,
			   robust => 0,
			   pseudo => 0
			 },
	   };

sub l1 {
##  encode("iso-8859-1",shift);
  return shift;
}

foreach my $file (@files) {
  $info->{files}++;
  open(FILE,"<$file") 
    || die "can't open $file: $!";
  while (<FILE>) {
    if (/<citation /) {
      $info->{citations}{nb}++;
      / pred="(.*?)"/ && $info->{citations}{pred}{l1($1)}++;
      / orig="sxpipe"/ && $info->{citations}{sxpipe}++;
      / mode="neg"/ && $info->{citations}{neg}++;
    }
    if (/<agent /) {
      $info->{citations}{agent}{nb}++;
      />(.*?)</ && $info->{citations}{agent}{dist}{l1($1)}++;
      / entity=/ && $info->{citations}{agent}{entity}++;
      /indirect_entity=/ && $info->{citations}{agent}{indirect_entity}++;
    }
    if (/<theme /) {
      $info->{citations}{thema}++;
    }
    if (/<audience /) {
      $info->{citations}{audience}++;
    }
    if (/<date /) {
      $info->{citations}{date}++;
    }
    if (/<entity /) {
      $info->{entities}{nb}++;
      / name="(.*?)"/ && $info->{entities}{dist}{l1($1)}++;
      / type="(.*?)"/ && $info->{entities}{type}{$1}++;
    }
    if (/<occ /) {
      $info->{entities}{occ}++;
      / anaphora=/ && $info->{entities}{anaphora}++;
    }
    if (/<dependencies /) {
      $info->{sentences}{nb}++;
      / mode="(.*)"/ && $info->{sentences}{$1}++;
    }
    if (/<verbatim /) {
      $info->{verbatim}++;
    }
  }
  close(FILE);
}

## print Dumper($info);
my $yaml = Dump($info);

print $yaml;
print "\n";
