#!/usr/bin/perl

use strict;
use HTTP::Request::Common;
require LWP::UserAgent;
use IO::Uncompress::Bunzip2 qw/bunzip2 $Bunzip2Error/ ;
my $sentences = shift;

my $ua = LWP::UserAgent->new;


my $host = shift || "localhost:9087";

my $response = $ua->post("http://$host/process",
			 Content_Type => 'form-data',
			 Content => [
				     news => [$sentences],
				     nocache => 1
				    ]
			);

if ($response->is_success) {
 ## print $response->decoded_content;  # or whatever
  if (0) {
    foreach my $type (qw/Content-type Content-Encoding Content-Length/) {
      print $response->header($type),"\n";
    }
  }
  if (0) {
    my $input = $response->content;
    my $z = new IO::Uncompress::Bunzip2 \$input
      or die "bunzip2 failed: $Bunzip2Error\n";
    while (<$z>) {
      print $_;
    }
  } else {
    print $response->content;
  }
}
else {
  die $response->status_line;
}
