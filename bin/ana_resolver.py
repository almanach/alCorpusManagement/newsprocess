#!/usr/bin/env python

import sys
import xml.etree.cElementTree as ET
from anaphora.news_reader import NewsSource
from anaphora.ana_detection import AnaphorDetector
from anaphora.resolution import Resolver

# load source from stdin
source = NewsSource( sys.stdin )

# detect anaphors
detector = AnaphorDetector( source )
anaphors = detector.detect()

# resolve anaphors to one entity
resolver = Resolver( anaphors, source.entities )
resolver.resolve()

# dump updated entities to stdout
ET.dump( source.entities_elt )


