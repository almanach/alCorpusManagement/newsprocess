#!/usr/bin/perl

use strict;
use warnings;
use Carp;

use AppConfig qw/:argcount :expand/;

my $config = AppConfig->new( "host|h=s" => {DEFAULT => 'localhost'},
			     "cache!" => {DEFAULT => "1"},
			     "parse!" => {DEFAULT => "1"},
			     "results=f",
			     "verbose|v!"        => {DEFAULT => 1},
			     "timeout=i"  => {DEFAULT => 240}
			   );

$config->args();

my $news = shift;

$news = "TX-SGE-$news" unless ($news =~ /-/);

my $host = $config->host;
my $url = "http://${host}:9087/process";
my @options = ();

push(@options,'-Fnocache=1') unless ($config->cache);
push(@options,'-Fnoparse=1') if ($config->parse);
push(@options,"-m",$config->timeout) if ($config->timeout);

foreach my $file (glob("*/*${news}*.xml")) {
  print STDERR "processing $news => $file\n";
  my $cmd  = "curl @options -Fnews=\@$file $url | recode -f u8..l1";
  exec($cmd);
}
