'''
Class for Quote
'''

import re


class Quote:

    def __init__(self, element):
        # id
        self.id = element.get('id')
        # text
        self.text = element.text
        # position
        self.par_id = element.get('where')
        self.par_idx = int(self.par_id.split('_')[1])
        self.extent = map(int, (element.get('start'),element.get('end')))
        self.pos = (self.par_idx,self.extent)
        return

    def __repr__(self):
        return "Quote %s: '%s' at %s" %(self.id,self.text,self.pos)
