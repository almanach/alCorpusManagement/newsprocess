#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

'''
Class for Occurrence
'''

'''
TODO:
    - check GF assignment esp. for obl
    - caching of GF values
    - back-off GF values when no parse
    - in citation attribute
'''

import re


class Occurrence:

    def __init__(self, element, document):
        self.document = document
        # xml
        self.form = element.get('alias').encode("latin-1")
        self.token_ids = element.get('tokens').split()
        # tokens
        self.tokens = [self.document.tokens.get(tid) for tid in self.token_ids]
        # position
        first_token,last_token = self.tokens[0],self.tokens[-1]
        self.par_id = first_token.par_id
        self.par_idx = first_token.par_idx
        self.s_id = first_token.s_id
        self.s_idx = first_token.s_idx
        self.extent = first_token.extent[0],last_token.extent[-1]
        self.pos = (self.par_idx,self.extent)
        self.token_span = (first_token.id,last_token.id)
        # set parse node
        self.set_parse_node()
        # set morphological features
        self.set_morpho()
        # gender
        self.gender = element.get('gender')
        if self.gender == 'f':
            self.gender = 'fem'
        elif self.gender == 'm':
            self.gender = 'masc'
        # GF
        self.set_gf()
        return


    def set_parse_node(self):
        self.node = None
        parse = self.document.parses.get(self.s_id)
        if parse:
            self.node = parse.find_node(self.token_span)
        return


    def set_morpho(self):
        self.subj,self.obj,self.obl = None,None,None
        self.cat = None
        agr_features = {}
        if self.node:
            self.cat = self.node.cat
            agr_features = self.node.get_agr_features()
        self.gender = agr_features.get('gender') 
        self.number = agr_features.get('number')
        self.person = agr_features.get('person')
        return


    def set_gf(self):
        self.gf = None
        if self.node:
            if self.node.is_main_subj():
                self.gf = 'subj'
            elif self.node.is_main_obj():
                self.gf = 'obj'
            elif self.node.is_main_obl():
                self.gf = 'obl'
        return


    def quote(self):
        # FIXME: this is so hacky!!!
        my_quote = False
        for qid in self.document.quotes:
            quote = self.document.quotes[qid]
            if quote.par_id != self.par_id:
                continue
            if quote.extent[0] <= self.extent[0] and self.extent[1] <= quote.extent[1]:
                my_quote = qid
                break
        return my_quote



    def __repr__(self):
        return "Occ '%s' at %s AGR:%s|%s|%s GF:%s" \
               %(self.form,self.pos,\
                 self.gender,self.number,self.person,self.gf)




