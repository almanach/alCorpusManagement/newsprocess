#!/usr/bin/env python

'''
Class for AnaDetector: extraction of anaphoric pronouns from document

Only third person singular pronouns for now.

TODO:
    - other pronouns
    - definite descriptions

'''

import sys
from anaphor import Anaphor, third_pers_sg_pro


class AnaphorDetector:

    def __init__(self, document):
        self.document = document
        return


    def detect(self):
        anaphors = []
        tokens = self.document.tokens.values()
        tokens.sort(key=lambda x:x.pos)
        for token in tokens:
            if third_pers_sg_pro.match( token.form ): # il, elle, lui
                parse = self.document.parses.get( token.s_id )
                if parse:
                    node = parse.find_node((token.id,token.id))
                    if node:
                        # filter pleonastics
                        if node.in_edge and node.in_edge.label == 'impsubj':
                            continue
                ana = Anaphor( token, self.document )
                anaphors.append( ana )
        return anaphors


    # detect from node
    # def detect(self):
#         anaphors = []
#         for sid in self.document.sentences:
#             sentence = self.document.sentences[sid]
#             parse = self.document.parses.get(sid)
#             if parse:
#                 # find cln|cla|cld
#                 for nid in parse.nodes:
#                     node = parse.nodes[nid]
#                     if node.cat in ['cln','cla','cld']:
#                         # filter pleonastics
#                         if node.in_edge and node.in_edge.label == 'impsubj':
#                             continue
#                         ana = Anaphor( node, self.document )
#                         # filter non 3rd pers sg (for now)
#                         if ana.person <> '3' or ana.number <> 'sg':
#                             continue
#                         anaphors.append( ana )
#             else:
#                 print >> sys.stderr, "No parse for sentence %s" %sid
#         return anaphors



if __name__=="__main__":


    import re
    from news_reader import NewsSource
    import xml.etree.cElementTree as ET
    
    news_xml = sys.argv[1]
    xml_tree = ET.ElementTree()
    xml_tree.parse(news_xml)
    aggregate = xml_tree.getroot()
    creation_time = aggregate.get('creationTime')
    date = tuple(map(int, re.search(r'(\d+)\-(\d+)\-(\d+)', creation_time).groups()))
    if date and date > (2009,06,01):
        source = NewsSource(news_xml)
        detector = AnaphorDetector(source)
        anaphors = detector.detect()
        for ana in anaphors:
            print ana


