#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-

'''
Class for Anaphor
'''

'''
TODO:
    - robust detection from tokens: OK
    - anaphor and occ should be subclassing
    - citation: e.g. in/out citation; agent of citations 
    - non 3rd sg pronouns
    - non clitic pronouns

'''

import re


# third_pers_pro = re.compile(r"^((.+['-])?(ils?|elles?)|le|la|lui|l')$")
third_pers_sg_pro = re.compile(r"^((.+['-])?(il|elle)|lui)$") # skip on,le,la,l'
sg_pro = re.compile(r"^(j'|je|t'|tu|(.+['-])?(il|elle)|le|la|lui|l')$")
# pl_pro = re.compile(r"^(nous|ils|elles|les)$")
masc_pro = re.compile(r"^((.+['-])?ils?|le)$")
fem_pro = re.compile(r"^((.+['-])?elles?|la)$")




class Anaphor:

    def __init__(self, token, document):
        self.token = token
        self.document = document
        self.set_form()
        self.set_pos()
        self.set_morpho()
        self.set_gf()
        return

    #### setters ####################################
    
    def set_form(self):
        # lexical attributes
        self.form = self.token.form
        return

    def set_pos(self):
        # positional attributes
        self.par_id = self.token.par_id
        self.par_idx = self.token.par_idx
        self.s_id = self.token.s_id
        self.s_idx = self.token.s_idx
        self.extent = self.token.extent[0],self.token.extent[-1]
        self.token_span = (self.token.id,self.token.id)
        self.pos = (self.par_idx,self.extent)
        return
        
    def set_morpho(self):
        # morpho-syntactic attributes
        self.gender = None
        self.number = None
        self.person = None
        # if third_pers_pro.match(self.form):
#             self.person = "3"
#         else:
#             self.person = "^3"
        if sg_pro.match(self.form):
            self.number = 'sg'
        else:
            self.number = 'pl'
        if masc_pro.match(self.form):
            self.gender = 'masc'
        elif fem_pro.match(self.form):
            self.gender = 'fem'
        return


    def set_gf(self):
        # from parse node
        self.gf = None
        parse = self.document.parses.get(self.s_id)
        if parse:
            node = parse.find_node(self.token_span)
            if node:
                if node.is_main_subj():
                    self.gf = 'subj'
                elif node.is_main_obj():
                    self.gf = 'obj'
                elif node.is_main_obl():
                    self.gf = 'obl'
        return

    ##### accessors ####################################

    def precedes( self, occ ):
        return self.pos < occ.pos

    def follows( self, occ ):
        return self.pos > occ.pos
    
    # distance features
    def sent_dist( self, occ ):
        return abs(self.s_idx-occ.s_idx)

    def par_dist( self, occ ):
        return abs(self.p_idx-occ.p_idx)

    #def npDist( self, occ ):
    #    return 

    #def wdDist( self, occ ):
    #    return

    #def hobbesDist( self, occ ):        
    #    return

    # morphological agreement/compatibility
    def number_agree( self, occ ):
        if self.number and occ.number:
            return self.number == occ.number 

    def gender_agree( self, occ ):
        if self.gender and occ.gender:
            return self.gender == occ.gender 

    def person_agree( self, occ ):
        if self.person and occ.person:
            return self.person == occ.person 

    def morph_agree( self, occ ):
        nagr = self.number_agree( occ )
        gagr = self.gender_agree( occ )
        pagr = self.person_agree( occ )
        return nagr and gagr and pagr 

    def number_compatible( self, occ ):
        if not (self.number and occ.number):
            return True
        return self.number == occ.number 

    def gender_compatible( self, occ ):
        if not (self.gender and occ.gender):
            return True
        return self.gender == occ.gender 

    def person_compatible( self, occ ):
        if not (self.person and occ.person):
            return True
        return self.person == occ.person 

    def morph_compatible( self, occ ):
        nagr = self.number_compatible( occ )
        gagr = self.gender_compatible( occ )
        pagr = self.person_compatible( occ )
        return nagr and gagr and pagr

    def quote(self):
        # FIXME: this is so hacky!!!
        my_quote = False
        for qid in self.document.quotes:
            quote = self.document.quotes[qid]
            if quote.par_id != self.par_id:
                continue
            if quote.extent[0] <= self.extent[0] and self.extent[1] <= quote.extent[1]:
                my_quote = qid
                break
        return my_quote

    def __repr__(self):
        return "Anaphor '%s' at %s AGR:%s|%s|%s GF:%s" \
               %(self.form,self.pos,\
                 self.gender,self.number,self.person,self.gf)
