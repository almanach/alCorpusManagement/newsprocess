'''
Class for Token
'''

import re


class Token:

    def __init__(self, element):
        # id
        self.id = element.get('id')
        # form
        self.form = element.get('form').encode("latin-1")
        # position
        self.par_id = element.get('where')
        self.par_idx = int(self.par_id.split('_')[1])
        self.s_id = re.search("(E\d+)F\d+",self.id).group(1)
        self.s_idx = int(self.s_id.lstrip('E'))
        self.extent = map(int, (element.get('start'),element.get('end')))
        self.pos = (self.par_idx,self.extent)
        return

    def __repr__(self):
        return "Token %s: '%s' at %s" %(self.id,self.form,self.pos)



