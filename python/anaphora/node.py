'''
Class for Node
'''

class Node:

    def __init__(self, elt, parse):
        self.elt = elt
        self.parse = parse
        self.id = elt.get('id')
        self.cluster = elt.get('cluster')
        self.cat = elt.get('cat')
        self.form = elt.get('form').encode('latin-1')
        self.lemma = elt.get('lemma').encode('latin-1')
        self.tree = elt.get('tree')
        self.deriv = elt.get('deriv')
        self.out_edges = []
        self.in_edge = None
        return


    def is_root(self):
        return not self.in_edge


    def is_empty(self):
        return not self.form


    def get_parent(self):
        if self.is_root():
            return None
        return self.in_edge.source_node


    def get_children(self):
        children = []
        for edge in self.out_edges:
            children.append(edge.target_node)
        return children


    def is_main_subj(self):
        parent = self.get_parent()
        if parent and parent.is_root():
            if self.in_edge.label == 'subject':
                return True


    def is_main_obj(self):
        parent = self.get_parent()
        if parent and parent.is_root():
            if self.in_edge.label == 'object':
                return True


    def is_main_obl(self):
        parent = self.get_parent()
        if parent and parent.cat == "prep":
            great_parent = parent.get_parent()
            if great_parent and great_parent.is_root():
                if self.in_edge.label == 'preparg':
                    return True


    def get_descendants(self):
        descendants = []
        for c in self.get_children():
            descendants.append(c)
            descendants.extend(c.get_descendants())
        return descendants
            

    def get_ancestors(self):
        ancestors = []
        if not self.is_root():
            parent = self.get_parent()
            ancestors.append(parent)
            ancestors.extend(parent.get_ancestors())
        return ancestors


    def get_span(self):
        cluster = self.parse.clusters.get(self.cluster)
        return cluster.get('left'), cluster.get('right')
    

    def get_projection(self):
        # NB: clitics do not have projection
        op = None
        if self.deriv:
            deriv = self.parse.derivs.get(self.deriv)
            if deriv:
                op = deriv.get('op')
            if not op: 
                derivs = self.deriv.split()
                if len(derivs) > 1:
                    for d in derivs:
                        deriv = self.parse.derivs.get(d)
                        if deriv:
                            op = deriv.get('op')
                            if op:
                                break
        return op
    

    def get_fs(self):
        fs = {}
        op_elt = self.get_projection()
        if op_elt: 
            fs_elt = op_elt.find('.//fs')
            for f_elt in fs_elt.getchildren():
                fname = f_elt.get('name')
                for c_elt in f_elt.getchildren():
                    if c_elt.tag == 'val':
                        fvalue = c_elt.text.strip()
                    else:
                        fvalue = c_elt.tag
                    fs[fname] = fvalue
        return fs


    def get_agr_features(self):
        fs = self.get_fs()
        agr_features = dict([(f,fs.get(f)) for f in ['gender','number','person']])
        return agr_features


    def get_token_ids(self):
        cid = self.parse._node2cluster.get(self.id) 
        return self.parse._cluster2tokens.get(cid)


    def __repr__(self):
        return "%s|%s|%s|%s" %(self.id,self.cat,self.form,self.lemma)
    
