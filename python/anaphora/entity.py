#!/usr/bin/env python
# -*- coding: iso-8859-1 -*-


'''
Class for Entity
'''

import re
import xml.etree.cElementTree as ET
from occurrence import Occurrence


class Entity:

    def __init__(self, element, document):
        self.document = document 
        # xml attributes
        self.element = element
        self.id = element.get('id')
        self.type = element.get('type')
        self.name = element.get('name').encode("latin-1")
        self.geoname = element.get('geoname')
        # self.gender = element.get('gender') # ignore since crappy
        # occurrences
        self.occurrences = {}
        for occ_elt in element.findall('.//occ'):
            if occ_elt.get('anaphora'): # do not anaphors from previous cache
                continue
            occ = Occurrence(occ_elt,self.document)
            self.occurrences[occ.pos] = occ
        self.occurrences = self.occurrences.values()
        self.occurrences.sort(key=lambda x:x.pos)
        # set agr features based on agr features from occurrences (by
        # majority rule)
        self.set_agr_features()
        # partial occurrence to be dynamically set during resolution
        self.realized_occs = [] 
        return


    def set_agr_features(self):
        self.set_gender()
        self.set_number()
        self.set_person()
        return


    def set_gender(self):
        self.gender = None
        # take majority gender in occurrences, unless strong indicators (e.g., M./Mme)
        gender_cts = {}
        for occ in self.occurrences:
            if re.search(r"^(m(ada)?me|dame|soeur|la\s+(ministre|d�put�e|pr�sidente))", occ.form, re.I):
                self.gender = 'fem'
                return
            if re.search(r"^(mr?\.|monsieur|mgr|monseigneur|le\s+(ministre|d�put�|pr�sident))", occ.form, re.I):
                self.gender = 'masc'
                return
            if occ.gender:
                gender_cts[occ.gender] = gender_cts.get(occ.gender,0) + 1
        gender_cts = gender_cts.items()
        if gender_cts:
            gender_cts.sort(key=lambda x:x[1])
            self.gender = gender_cts[-1][0]
        return


    def set_number(self):
        self.number = None
        number_cts = {}
        for occ in self.occurrences:
            if occ.number:
                number_cts[occ.number] = number_cts.get(occ.number,0) + 1
        number_cts = number_cts.items()
        if number_cts:
            number_cts.sort(key=lambda x:x[1])
            self.number = number_cts[-1][0]
        return
    

    def set_person(self):
        self.person = None
        person_cts = {}
        for occ in self.occurrences:
            if occ.person:
                person_cts[occ.person] = person_cts.get(occ.person,0) + 1
        person_cts = person_cts.items()
        if person_cts:
            person_cts.sort(key=lambda x:x[1])
            self.person = person_cts[-1][0]
        return


    def first_occ(self):
        return self.occurrences[0]


    def last_occ(self):
        return self.occurrences[-1]


    def last_realized_occ(self):
        return self.realized_occs[-1]


    def occ_count(self):
        return len(self.occurrences)


    def realized_occ_count(self):
        return len(self.realized_occs)


    def sent_span(self):
        return self.first_occ().s_pos,self.last_occ().s_pos 
    
    
    def get_aliases(self):
        return [occ.alias for occ in self.occurrences]



    def update(self, anaphor):
        # create container
        new_occ_elt = ET.SubElement(self.element, "occ")
        new_occ_elt.set("alias", anaphor.form)
        new_occ_elt.set("start", str(anaphor.extent[0]))
        new_occ_elt.set("end", str(anaphor.extent[1]))
        new_occ_elt.set("where", anaphor.par_id)
        new_occ_elt.set("tokens", str(anaphor.token.id))
        new_occ_elt.set("anaphora","1")
        # add occurrence
        # new_occ = Occurrence(new_occ_elt,self.document)
        self.occurrences.append( anaphor )
        # re-sort occurrences on position
        self.occurrences.sort(key=lambda x:x.pos)
        # recompute agr features for entity
        self.set_agr_features()
        return

    def __repr__(self):
        return "Entity %s (%s): %s %s|%s|%s" %(self.id,self.type,self.occurrences,
                                               self.gender,self.number,self.person)
    

