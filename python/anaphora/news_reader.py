#!/usr/bin/env python

import xml.etree.cElementTree as ET
import re
from token import Token
from quote import Quote
from entity import Entity
from parse import Parse
from collections import defaultdict



class NewsSource:

    def __init__(self, filepath):
        self.path = filepath
        # text content
        self.content = {}
        # tokens
        self.tokens = {}
        # quotes
        self.quotes = defaultdict(dict)
        # entities
        self.entities_elt = None
        self.entities = {}
        # sentences and parses
        self.sentences = defaultdict(list)
        self.parses = {}
        self.load()
        self.process()
        return


    def load(self):
        self.xml_tree = ET.ElementTree()
        self.xml_tree.parse( self.path )
        return


    def process(self):
        # text content
        hl = self.xml_tree.find('.//HeadLine')
        self.handle_content(hl)
        for p in self.xml_tree.findall('.//DataContent/p'):
            self.handle_content(p)
        # tokens 
        for t in self.xml_tree.findall('.//token'):
           self.handle_token(t)
        # quotes
        for q in self.xml_tree.findall('.//verbatim'):
           self.handle_verbatim(q) 
        # dependencies
        for d in self.xml_tree.findall('.//dependencies'):
            parse = Parse( d )
            self.parses[parse.id] = parse
        # entities
        self.entities_elt = self.xml_tree.find('.//entities')
        for e in self.xml_tree.findall('.//entity'):
            self.handle_entity(e)
        return


    def handle_content(self, element):
        _id = element.get('id')
        if _id:  # note: some paragraphs don't have id!
            self.content[_id] = {
                'id': _id,
                'text': element.text.encode("latin-1")
                }
        return


    def handle_token(self, element):
        token = Token( element )
        self.tokens[token.id] = token
        self.sentences[token.s_id].append(token) # need sorting
        return


    def handle_verbatim(self, element):
        quote = Quote( element )
        self.quotes[quote.id] = quote 
        return


    def handle_entity(self, element):
        entity = Entity( element, self )
        self.entities[entity.id] = entity
        return


    def __repr__(self):
        return "CONTENT: %s\n\nTOKENS: %s\n\nENTITIES: %s" \
               %(self.content, self.tokens, \
                 "\n".join(["%s: %s" %(eid,ent) for (eid,ent) in self.entities.items()]))


if __name__=="__main__":

    import sys
    
    news_xml = sys.argv[1]
    source = NewsSource(news_xml)
    print source
