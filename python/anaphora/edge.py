'''
Class for Edge
'''

class Edge:

    def __init__(self, elt, parse):
        self.elt = elt
        self.parse = parse
        self.id = elt.get('id')
        self.label = elt.get('label')
        self.type = elt.get('type')
        source_nid = elt.get('source')
        target_nid = elt.get('target')
        self.source_node = self.parse.nodes[source_nid]
        self.target_node = self.parse.nodes[target_nid]
        self.source_node.out_edges.append( self )
        self.target_node.in_edge = self
        return

    def __repr__(self):
        return "%s --%s|%s--> %s" %(self.source_node,
                                    self.label,
                                    self.type,
                                    self.target_node)


