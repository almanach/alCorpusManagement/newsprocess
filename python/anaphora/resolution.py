#!/usr/bin/env python

'''
Class for Resolver
'''

import sys
from copy import deepcopy

'''
TODO:
    - GF guessing when no parse
    - binding principles
    - improve gender detection with guesser 
    - use citation info: OK (sort of)
    - tune weights
'''


import sys



class Resolver:


    def __init__(self, anaphors, entities, verbose=1):
        self.anaphors = anaphors
        self.entities = entities.values()
        self.verbose = verbose
        return


    def resolve(self):
        ''' resolution is based on hard constraints (e.g.,
        morpho-syntax) and soft preferences (e.g. recency, repetition,
        GRs)'''
        for ana in self.anaphors:
            self.write_log(0,"\n>>> %s" %ana)
            # consider only occurrences that are realized before the
            # anaphor
            total_realized_occ_ct = 0
            for cand in self.entities:
                cand.realized_occs = [occ for occ in cand.occurrences
                                     if ana.follows(occ)]
                total_realized_occ_ct += len(cand.realized_occs)
            # apply hard filters
            candidates = self.apply_filters( ana, self.entities)
            self.write_log(1,"%s/%s candidates after hard filerting" %(len(candidates),len(self.entities)))
            # score candidates based on preferences
            scored_cands = []
            for cand in candidates:
                score = self.score( ana, cand, total_realized_occ_ct)
                scored_cands.append( (cand, score) )
            # rank candidates
            scored_cands.sort(key=lambda x:x[1])
            self.write_log(1,"Ranked candidates: %s" %scored_cands) 
            if scored_cands <> []:
                best_cand = scored_cands[-1][0]
                self.write_log(0,"==> %s" %best_cand)
                # update antecedent entity
                best_cand.update( ana )
                self.write_log(1,"Updated entity: %s" %best_cand)
        return



    def apply_filters(self, ana, entities):
        cands = []
        for ent in entities:
            # filter entities that appear after anaphor (i.e., all its
            # occ's come after the anaphor) => no cataphora
            if ana.pos < ent.first_occ().pos:
                self.write_log(2,"Pos. filter %s" %ent)
                continue
            # filter entities that are incompatible in
            # gender, number, person
            if not ana.morph_compatible( ent ):
            # if not ana.morph_agree( ent ):
                self.write_log(2,"Morph. Filter %s" %ent)
                continue
            # filter entities that are incompatible in type
            if not ent.type in ['person']: #,'np']:
                self.write_log(2,"Sem. Filter %s" %ent)
                continue
            cands.append( ent )
        return cands



    def score(self, ana, entity, total_realized_occ_ct):
        ''' scoring scheme a la Lappin and Leass

        sentence recency: 100 
        subject:           80
        object:            60
        i_object:          40

        values are cut in half at each new sentence

        also add weight for # of mentions (= percentage of total # of
        mentions)
        '''

        self.write_log(1,"=?=> Candidate %s" %entity)
        
        score = 0
        
        # compute sentence distance based on last occurrence
        last_occ = entity.last_realized_occ()
        s_dist = ana.sent_dist( last_occ )

        # recency weighting
        recency_weight = self.weighting( s_dist, w=100 )
        self.write_log(1,"Recency weight: %s" %recency_weight)
        score += recency_weight
        
        # GFs weighting
        gf_weight = 0
        if last_occ.gf == 'subj':
            gf_weight = self.weighting( s_dist, w=80 )
        elif last_occ.gf == 'obj':
            gf_weight = self.weighting( s_dist, w=60 )
        elif last_occ.gf == 'obl': 
            gf_weight = self.weighting( s_dist, w=40 )
        self.write_log(1,"GF weight: %s" %gf_weight)
        score += gf_weight

        # salience (approx. by # of occ's)
        salience_weight = 100*len(entity.realized_occs)/float(total_realized_occ_ct)
        score += salience_weight
        self.write_log(1,"Salience weight: %s" %salience_weight)

        # down-weight entities whose last occ is in a quote (provided
        # the pronoun is not in a quote)
        if last_occ.quote() and not ana.quote():
            self.write_log(1,"Quote down-weighting: -100")
            score -= 100

        return score



    def weighting(self, s_dist, w=100):
        w = float(w)
        while s_dist <> 0:
            w /= 2
            s_dist -= 1
        return w

    
    def write_log(self, verbose, stuff, where=sys.stderr):
        if verbose < self.verbose:
            print >> where, stuff


        

if __name__ == '__main__':

    import sys
    import optparse
    from news_reader import NewsSource
    from ana_detection import AnaphorDetector
    from resolution import Resolver

    usage = "usage: %prog [options] [file]"
    parser = optparse.OptionParser(usage=usage)
    parser.add_option("-v", "--verbosity", type=int, default=1)
    (options, args) = parser.parse_args()

    # load source from stdin
    source = NewsSource( args[0] )

    # detect anaphors
    detector = AnaphorDetector( source )
    anaphors = detector.detect()

    # resolve anaphors to one entity
    resolver = Resolver( anaphors, source.entities, verbose=options.verbosity )
    resolver.resolve()
