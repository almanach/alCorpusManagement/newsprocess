package NewsProcess::Anaphore;

use strict;
use XML::Twig::XPath;
use IPC::Open2;

sub process {
  my $class = shift;
  my $cas = shift;
  my $document = $cas->document;	# the NewML document
  my $root = $cas->root;
  my $xml = $cas->xml;

  my $home = $cas->home;
  $ENV{"PYTHONPATH"} = "$home/python:$ENV{PYTHONPATH}";
  $ENV{"PATH"} = "$home/bin:$ENV{PATH}";

  ##  $cas->log(1,"Path in anaphora: $ENV{PATH}");

  my $pid = open2(*OUT,*IN,'ana_resolver.py')
    || die "can't run ana_resolver.py";
  
  $xml->set_output_encoding('UTF-8');
  $xml->set_encoding('UTF-8');
  $xml->print(\*IN);
  close(IN);
  
  if (my $entities = XML::Twig::XPath->new()->safe_parse(\*OUT)->root) {
    ##    $cas->log(1,"anaphora results ".$entities->sprint);
    $entities->cut;
    my $old = $root->first_child('entities');
    $entities->replace($old);
    my %tokens = ();
    foreach my $en ($entities->children('entity')) {
      foreach my $occ ($en->children('occ')) {
	next unless ($occ->att('anaphora'));
	##	my $txt = $occ->att('alias');
	my $tokens = $occ->att('tokens');
	my $pos = NewsProcess::Pos::xml2pos($cas,$occ);
	my $txt = $pos->txt;
	$cas->log(3,"processing anaphore '$tokens' : '$txt'".$pos->sprint);
	if ($txt =~ s/^(s'|qu'|.+-)(il|elle|ils|elles)$/$2/) {
	  $cas->log(3,"resize anaphora occurrence to '$txt', removed '$1'");
	  my $l = length($1);
	  $occ->set_att(start => $occ->att('start')+$l);
	} elsif ($txt =~ s/^(l)'(.*)$/$1/) {
	  $cas->log(3,"resize anaphora occurrence to '$txt', removed '$2'");
	  my $l = length($2);
	  $occ->set_att(end => $occ->att('end')+$l);
	}

	$tokens{$occ->att('tokens')} = $occ;
      }
    }
    if (%tokens) {
      foreach my $node (values %{$cas->{nodes}}) {
	my $cat = $node->cat;
	next unless (grep {$cat eq $_} qw/pro cln cla cld prel/);
	my $cluster = $cas->id2obj($node->cluster);
	my $tokens = $cluster->att('lex');
	$tokens = join(' ',map {/^(E\d+F\d+)\|/ && $1} split(/\s+/,$tokens));
	##	$cas->log(3,"Test node lex='$tokens' ".$node->sprint);
	if (my $occ = $tokens{$tokens}) {
	  $cas->log(2,"tagging cluster ".$node->cluster);
	  $cluster->set_att('#entity_occ' => $occ);
	}
      }
    }
  }
  close(OUT);
}

1;

__END__

=head1 NAME

NewProcess::Anaphore - Detection of anaphora within AFP news

=head1 SYNOPSIS

  use NewProcess::Anaphore;

  NewsProcess::Anaphore::process($cas);

=head1 DESCRIPTION

NewProcess::Anaphore - Anaphora resolution within a NewsXML document.

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
Pascal Denis <Pascal.Denis@inria.fr>

=cut
