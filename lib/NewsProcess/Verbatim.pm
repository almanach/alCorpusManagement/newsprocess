package NewsProcess::Verbatim;

use strict;
use XML::Twig;
use NewsProcess::Pos;
use Encode;

sub process {
  my $class = shift;
  my $cas = shift;
  my $verbatims = $cas->add_container('verbatims');
  my $casverbs = $cas->{verbatims} = {};
  foreach my $p (@{$cas->{content}}) {
    my $where = $p->att('id');
    my $str = $p->trimmed_text;
  L:
    if ($str =~ /\G.*?(?=\")/sgc) {
      my $start = pos($str);
      ##      $cas->log(2,"verbatim $where: start at $start");
      if ($str =~ /\G(".+?")/sgc) {
	my $txt = "$1";
	my $end = pos($str);
	##	$cas->log(2,"verbatim $where: end at $end, txt=$txt");
	my $verbatim = XML::Twig::Elt->new( 'verbatim' => {}, $txt );
	$verbatim->set_direct_pos($where,$start,$end);
	$verbatim->paste(last_child => $verbatims);
	$cas->register($verbatim);
	$casverbs->{$where}{$start}{$end} = $verbatim;
	goto L;
      } else {
	$cas->log(1,"*** warning unclosed verbatim from $start in $where");
      }
    }
  }
}

1;

__END__

=head1 NAME

NewProcess::Verbatim - Extract verbatim segments from news, assuming
they are correctly balanced

=head1 SYNOPSIS

  use NewProcess::Verbatim;

  NewsProcess::Verbatim::process($xml);

=head1 DESCRIPTION

NewProcess::Verbatim - Extract verbatim segments from news, assuming
they are correctly balanced

The verbatim container has the following form
<verbatims>
  <verbatim id="verbatims_<id>" where="elt_id" start="0" end="u">content</verbatim>
  ...
</verbatims>

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
