package NewsProcess::Align;

use strict;
use XML::Twig;
use NewsProcess::Pos;
use Encode;

my %altforms = (
		'&lt;' => '<',
		'&gt;' => '>',
		'&amp;' => '&',
	       );


sub process {
  my $class = shift;
  my $cas = shift;

  my $xml = $cas->add_container('tokens');
  my $tokens = { xml => $xml,
		 ## tokens prepared during parsing
		 ## fetched from sxpipe output
		 cache => $cas->{tokens} || {},
	       };

  my $aligner = { src => [ @{$cas->{content}} ],
		  current => undef
		};
  my $current = $aligner->{current} =  shift @{$aligner->{src}};
##  my $str = $current ? encode('utf8',$current->trimmed_text) : '';
  my $str = $current ? encode('latin1',$current->trimmed_text) : '';
  $str =~ /^/sgc;
  my $l = length($str);
  my $where = $aligner->{current}->att('id');
  ## print STDERR "init l=$l '$str'\n";
  my $start;
  my $end;

  my @tokens = sort {token_sort($a,$b)} keys %{$tokens->{cache}};

  ## do a first pass, just to be sure the tokens are anchored
  foreach my $k (@tokens) {
    my $token = $tokens->{cache}{$k};
    $token->paste( last_child => $xml );
    $cas->register($token);
  }

  while (@tokens) {
    my $k = shift @tokens;
    my $token = $tokens->{cache}{$k};
    #    my $form = encode("latin1",$token->att('form'));
    my $form = $token->att('form');
    my $exp = $form;
    $form =~ s/\\([()%])/$1/og;
#    unless ($exp =~ /^-/) {
      ## a pb in sxpipe
      $exp =~ s/(-+)/\\E\\s*$1\\s*\\Q/og;
#    }
  L:
    $str =~ /\G\s*/sgc;
    $start = pos($str);
    ##    print STDERR "test with pos=$start l=$l form=$form and str=$str\n";
    if ($start == $l) {
      my $current = $aligner->{current} =  shift @{$aligner->{src}};
      unless ($current) {
	$cas->log(1,"*** Pb in aligment on $k '$form'");
	last;
      }
      $where = $current->att('id');
#      $str = encode('utf8',$current->trimmed_text);
      $str = encode('latin1',$current->trimmed_text);
      $l = length($str);
##      print STDERR "switched to l=$l '$str'\n";
      $str =~ /^/sgc;
      goto L;
    }
    my ($altform,$altform2) = alt_forms($form);
    unless ($str =~ /\G\Q$form\E/sgci
	    || ($altform && $str =~ /\G\Q$altform\E/sgci)
	    || ($altform2 && $str =~ /\G\Q$altform2\E/sgci)
	   )  {
      my $area = substr($str,$start,15);
      $cas->log(1,"*** Pb in aligment on $k '$form' at pos $start '$area' (try alternative)");
      ##      $str =~ /\G\S+/sgco;
      ## fail: we then try char by char, possibly with interleaving blanks
      my @chars = split(/\s*/,$form);
      foreach my $char (@chars) {
	$str =~ /\G\s*/sgci;
	if ($char eq "'" || $char eq '-' || $char eq '_') {
	  $str =~ /\G./sgci;
	} elsif ($str !~ /\G\Q$char\E/sgci) {
	  $cas->log(1,"*** really an aligment pb on $k '$form' at pos $start '$area'");
	  $str =~ /\G\S+/sgci;
	  ##	  last;
	  ## try to move to next token !
	  my @rest = @tokens;
	  my @ignored = ();
	  while (@rest) {
	    my $n = shift @rest;
	    my $next = $tokens->{cache}{$n};
	    my $nform = $next->att('form');
	    my $current = pos($str);
	    my $i = index($str,$nform,$current);
	    if ($i >= 0) {
	      pos($str) = $i;
	      unshift(@rest,$n);
	      @tokens = @rest;
	      last;
	    } else {
	      # this token is not going to be seen !
	      push(@ignored,$next);
	    }
	  }
	  my $end = pos($str);
	  foreach my $ignore (@ignored) {
	    $cas->log(2,"alignment ignore ".$ignore->att('id')." : setting default value");
	    $ignore->set_direct_pos($where,$end,$end);
	    $ignore->set_att(badalign => 1);
	  }
	  last;
	}
      }
    }
    $end = pos($str);
    $token->set_direct_pos($where,$start,$end);
  }
}

sub alt_forms {

    my $form = shift;

    my $altform;
    my $altform2;

    if ($form =~ /^PONCT(.+)$/o) {
	$altform = $1;
    } elsif ($form =~ /(oe|OE|ae|AE|'|-|&#\d+;)/o) {
	$altform = $form;
	$altform =~ s/&#(\d+);/chr($1)/ge;
    } elsif (my $tmp = $altforms{$form}) {
	$altform = $tmp;
    } elsif ($form =~ /\.\.\./) {
	$altform = $form;
    } elsif ($form =~ /\&(gt,lt|amp);/) {
	$altform = $form;
	$altform =~ s/(\&(?:gt|lt|amp);)/$altforms{$1}/ge;
    }
    
    if (($form =~ /[:'"-]/o) && ($form !~ m{\\$})) {
      ## \x{EFBFBD}
      $altform2 = $form;
      $altform2 =~ s/\*/\\*/og;
      $altform2 =~ s/\+/\\+/og;
      $altform2 =~ s/\?/\\?/og;
      $altform2 =~ s/\./\\./og;
      $altform2 =~ s/\(/\\(/og;
      $altform2 =~ s/\)/\\)/og;
      $altform2 =~ s/\[/\\]/og;
      $altform2 =~ s/:/(\\s*):(\\s*)/og;
      $altform2 =~ s/&#(\d+);/chr($1)/ge;
    }

    return $altform,$altform2;
}



sub xpointer {
  my ($w,$s,$e) = @_;
  my $d = $e-$s;
  return "xpointer(string-range(id('$w'),'',$s,$d))";
}

sub token_sort {
  my ($a,$b) = @_;
  my ($a1,$a2) = ($a =~ /E(\d+)F(\d+)/);
  my ($b1,$b2) = ($b =~ /E(\d+)F(\d+)/);
  return ($a1 <=> $b1) || ($a2 <=> $b2);
}

sub NewsProcess::CAS::cluster2pos {
  my $cas = shift;
  my $cluster = shift;
  my @ids = map {/^(E\d+F\d+)/ && $1} split(/\s+/,$cluster->att('lex'));
  unless (@ids) {
    $cas->log(1,'Pb pos en empty cluster ',$cluster->att('id'));
    return;
  }
  ##  $cas->log(5,"ids @ids");
  my $first = $cas->id2obj($ids[0]);
  my $last = $cas->id2obj($ids[-1]);
  ($first && $last) or return undef;
  return NewsProcess::Pos->new(
			       where => $first->att('where'),
			       start => $first->att('start'),
			       end => $last->att('end'),
			       cas => $cas
			      );
}

sub NewsProcess::CAS::span2pos {
  my $cas = shift;
  my $dep = shift;
  my $left = shift;
  my $right = shift;
  foreach my $cluster ($dep->findnodes("cluster[\@left='$left']")) {
    if ($cluster->att('lex')) {
      $left = $cas->cluster2pos($cluster);
      last;
    }
  }
  foreach my $cluster ($dep->findnodes("cluster[\@right='$right']")) {
    if ($cluster->att('lex')) {
      $right = $cas->cluster2pos($cluster);
      last;
    }
  }
  ($left && $right) or return undef;
  return NewsProcess::Pos->new(
			       where => $left->where,
			       start => $left->start,
			       end => $right->end,
			       cas => $cas
			      );
}

sub NewsProcess::CAS::node2pos {
  my $cas = shift;
  my $node = shift;
  $cas->cluster2pos($cas->id2obj($node->att('cluster')));
}

sub NewsProcess::CAS::op2pos {
  my $cas = shift;
  my $op = shift;
  $cas->span2pos($op->parent,split(/\s+/,$op->att('span')));
}

sub NewsProcess::CAS::sid2pos {
  my $cas = shift;
  my $elt = shift;
  my $dep = $elt->parent('dependencies');
  ## use memoization through private hidden attribute #span
  unless ($dep->att('#span')) {
    my @clusters = $dep->children('cluster');
    my $first;
    my $left;
    my $last;
    my $right;
    foreach my $cluster (@clusters) {
      next unless ($cluster->att('lex'));
      if ((!defined $first) || ($cluster->att('left') < $left)) {
	$first = $cluster;
	$left = $cluster->att('left');
      }
      if ((!defined $last) || ($cluster->att('right') > $right)) {
	$last = $cluster;
	$right = $cluster->att('right');
      }
    }
    $cas->log(4,"sid2obj first=$first last=$last");
    $dep->set_att('#span' => $cas->cluster2pos($first)->add($cas->cluster2pos($last)));
  }
  return $dep->att('#span');
}

sub XML::Twig::Elt::set_pos {
  my $elt = shift;
  my $pos = shift;
  $elt->set_att(where => $pos->where);
  $elt->set_att(start => $pos->start);
  $elt->set_att(end => $pos->end);
  return $elt;
}

sub XML::Twig::Elt::set_direct_pos {
  my $elt = shift;
  my ($where,$start,$end) = @_;
  $elt->set_att(where => $where);
  $elt->set_att(start => $start);
  $elt->set_att(end => $end);
  return $elt;
}

1;

__END__

=head1 NAME

NewProcess::Align - Align SxPipe tokens with original texts, building
a mapping token id/offset pairs.

=head1 SYNOPSIS

  use NewProcess::Align;

  NewsProcess::Align::process($xml);

=head1 DESCRIPTION

NewProcess::Align - Align SxPipe tokens with original texts, building
a mapping token id/offset pairs.

The mapping is represented by adding to $xml the following structure:
<tokens>
  <token id="E1F0" where="elt_id" start="0" end="u" form=""/>
  ...
</tokens>

Rather than using the 3 attributes 'where', 'start' and 'end', one
could use an xpointer-based notation attached to some attribute 'span'.

*** WARNING *** positions starts at 0, and fall between chars
pos    0 1 2 3 4
chars   a b c d

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
