package NewsProcess::Pos;
use Moose;

has 'where' => ( is => 'ro',
		 isa => 'Str',
		 documentation => 'which XML element Id'
	       );

has 'start' => ( is => 'ro',
		 isa => 'Int',
		 documentation => 'start offset in \'where\' element'
	       );

has 'end' => ( is => 'ro',
	       isa => 'Int',
	       documentation => 'end offset in \'where\' element'
	     );

has 'cas' => ( is => 'ro',
	       isa => 'NewsProcess::CAS',
	       weak_ref => 1,
	       documentation => 'CAS container'
	     );


sub xml2pos {
  my $cas = shift;
  my $xml = shift;
  return __PACKAGE__->new( where => $xml->att('where'),
			   start => $xml->att('start'),
			   end => $xml->att('end'),
			   cas => $cas);
}

sub txt {
  my $self = shift;
  my $cas = $self->cas;
  my $elt = $cas->id2obj($self->where);
  my $txt = $elt->trimmed_text;
  return  substr($txt,$self->start,$self->end-$self->start);
}

sub clean_txt {
  my $self = shift;
  my $cas = $self->cas;
  my $elt = $cas->id2obj($self->where);
##  $cas->log(4,"test pos ".$elt->att('id')." ".$elt->sprint);
  my $txt = $elt->trimmed_text;
##  $cas->log(4,"test pos2 ".$txt);
  $txt = substr($txt,$self->start,$self->end-$self->start);
  $txt =~ s/^[("'�]//so;
  $txt =~ s/[)"'�]$//so;
  return $txt;
}


sub normalize {
  ## modify the bound of a box to enclose only "interesting" stuff
  my $self = shift;
  my $text = $self->txt;
  my $start = $self->start;
  my $end = $self->end;
  if ($text =~ s/^([:.,()\"\'\�\�]+)//so) { 
    $start += length($1);
  }
  if ($text =~ s/([:.,()\"\'\�\�]+)$//so) { 
    $end -= length($1);
  }
  return __PACKAGE__->new( where => $self->where,
			   start => $start,
			   end => $end,
			   cas => $self->cas
			 );
}

sub extrude {
  ## remove $extrude's span from $self's span
  my $self = shift;
  my $extrude = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $extrude->where;
  my $xstart = $extrude->start;
  my $xend = $extrude->end;
  if ($xwhere eq $where) {
    my $rstart = ($xstart <= $start) ? $xend : $start;
    my $rend = ($xend >= $end) ? $xstart : $end;
    $rstart = $rend if ($rstart > $rend);
    $self->cas->log(3,"extruding ($xstart,$xend) from ($start,$end) => ($rstart,$rend)");
    return __PACKAGE__->new(where => $where, 
			    start => $rstart, 
			    end => $rend,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub left_border {
  my $self = shift;
  my $border = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $border->where;
  my $xstart = $border->start;
  my $xend = $border->end;
  if (($where eq $xwhere) 
      && ($start < $xend)
      && ($xend <= $end)) {
    return __PACKAGE__->new(where => $where, 
			    start => $xend, 
			    end => $end,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub right_border {
  my $self = shift;
  my $border = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $border->where;
  my $xstart = $border->start;
  my $xend = $border->end;
  if (($where eq $xwhere) 
      && ($xstart < $end)
      && ($start <= $xstart)) {
    return __PACKAGE__->new(where => $where, 
			    start => $start, 
			    end => $xstart,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub add {
  my $self = shift;
  my $add = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $add->where;
  my $xstart = $add->start;
  my $xend = $add->end;
  if ($xwhere eq $where) {
    my $rstart = ($xstart < $start) ? $xstart : $start;
    my $rend = ($xend > $end) ? $xend : $end;
    return __PACKAGE__->new(where => $where, 
			    start => $rstart, 
			    end => $rend,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub left_add {
  my $self = shift;
  my $add = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $add->where;
  my $xstart = $add->start;
  if ($xwhere eq $where) {
    my $rstart = ($xstart < $start) ? $xstart : $start;
    return __PACKAGE__->new(where => $where, 
			    start => $rstart, 
			    end => $end,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub verbatim_extend {
  my $self = shift;
  # if some span fall into some verbatim, we use this one
  my $cas = $self->cas;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  if (my $verbatims = $cas->root->first_child('verbatims')) {
    foreach my $verbatim ($verbatims->children('verbatim')) {
      my $xwhere = $verbatim->att('where');
##      $cas->log(3,"verb try: $where($start,$end) vs $xwhere"); 
      next unless ($xwhere eq $where);
      my $xstart = $verbatim->att('start');
      my $xend = $verbatim->att('end');
      $cas->log(3,"verb extend check: $where($start,$end) vs $xwhere($xstart,$xend)"); 
      if ((($xstart <= $start) && ($end <= $xend))
	  || (($xstart < $start) && ($start < $xend))
	  || (($end < $xend) && ($xstart < $end))
	 ) {
	return __PACKAGE__->new( where => $where,
				 start => ($xstart < $start) ? $xstart : $start,
				 end => ($xend > $end) ? $xend : $end,
				 cas => $cas
			       );
      }
    }
  }
  return $self;
}

sub right_add {
  my $self = shift;
  my $add = shift;
  my $where = $self->where;
  my $start = $self->start;
  my $end = $self->end;
  my $xwhere = $add->where;
  my $xend = $add->end;
  if ($xwhere eq $where) {
    my $rend = ($xend > $end) ? $xend : $end;
    return __PACKAGE__->new(where => $where, 
			    start => $start, 
			    end => $rend,
			    cas => $self->cas
			   );
  } else {
    return $self;
  }
}

sub sprint {
  my $self = shift;
  return $self->where.":[".$self->start.",".$self->end."]";
}

no Moose;
##__PACKAGE__->meta->make_immutable;

1;
