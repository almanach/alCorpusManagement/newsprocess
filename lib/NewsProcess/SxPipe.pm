package NewsProcess::SxPipe;

use strict;
use XML::Twig;
use IO::String;
use Encode;
use IPC::Run;
use IPC::Open2;

sub process {

	my $class = shift;
	my $cas = shift;

	my $document = $cas->document;	# the NewML document
	my $root = $cas->root;
	my $xc = $cas->{content} = [];
	my $database = 
	$cas->add_container('database');
	my @content = ();

	if (my $title = $document->first_descendant(qr/HeadLine/io)) {
		my $text = $title->trimmed_text;
		if ($text !~ /^\s*$/) {
			$cas->register($title);
			push(@$xc,$title);
			$text =~ s/\."/. "/sog;
			push(@content,split_text($text));
		}
	}

	my $signatures = $cas->add_container('signatures');
	foreach my $p ($document->descendants('p')) {
		## assume last <p> is for the signature
		#next if ($p->is_last_child);
		my $text = $p->trimmed_text;
		## test for signature (Rosa)
		if($text =~ m/^\s*[a-z�&A-Z]{2,4}((-|\/)[a-z�&A-Z]{2,4}\/?)+( +[a-z\.]{1,7})?\s*$/){
			$cas->log(1,"signature detected: ".$text);
			my $signature = XML::Twig::Elt->new('signature', $text);
			$signature->paste(last_child => $signatures);
			next;
		}
		## only select area with a at least a few letters
		next unless ($text =~ /[[:alpha:]]{2,}/);
		$cas->register($p);
		##		$p->set_att(foo => "�ric�");
		push(@$xc,$p);
		if ($p->is_first_child && ($text =~ s/^(.*?\(\s*AFP\s*\)\s*)(?=-)//)) {
			## first line usually starts with '_LOCATION _DATE (AFP) - ....'
			my $preamble = $1;
			push(@content,$preamble)
		}
		push(@content,split_text($text));
	}

	## @content = map {encode(latin1 => $_)} @content;

	my $content = join("\n\n",@content);
#	my $content = Encode::encode("utf8", $content);

#	$cas->log(3,"Executing SxPipe over content $content\n");

	my $parse_stats = $cas->{parse_stats} ||= {
		start => $cas->elapsed,
		disamb => 0,
		parse => 0,
		forest => 0
	};

	my $tmp = ' ' x 20000; # preallocate to be faster
	$tmp = '';

	# If preprocessings, replace $content
	my $sxpipe_cmdpre = $cas->config->{sxpipe}{cmdpre};
	if($sxpipe_cmdpre){
		#$cas->log(1,"sxpipe cmdpre=$sxpipe_cmdpre");
		while (1) {
			eval {
				my $pid = open2(*OUT,*IN,"${sxpipe_cmdpre}");
				print IN $content;
				close(IN);
				$tmp .= $_ while (<OUT>);
				close(OUT);
				waitpid $pid, 0;
			};
			if ($@) {
				$cas->log(1,"*** Pb sxpipe (pre): $@");
			} else {
				last;
			}
		}
		$cas->{dag_pre} = $tmp;
		$content = $tmp;
		$tmp = '';
	}
	
	my $sxpipe_cmd = $cas->config->{sxpipe}{cmd};
	#$cas->log(1,"sxpipe cmd=$sxpipe_cmd");
	while (1) {
		eval {
			my $pid = open2(*OUT,*IN,"${sxpipe_cmd}");
			print IN $content;
			close(IN);
			$tmp .= $_ while (<OUT>);
			close(OUT);
			waitpid $pid, 0;
		};
		if ($@) {
			$cas->log(1,"*** Pb sxpipe: $@");
		} else {
			last;
		}
	}

	$parse_stats->{spipe} = $cas->elapsed;

	#print STDERR "sxpipe-edylex out : $tmp\n\n";

	#$cas->log(5,$tmp);

	my $dag ="";
	my $cache = $cas->{tokens} ||= {};
	my $cacheforms = $cas->{forms} ||= {};
	my $sid = 1;
	my %stokens = ();
	my $fullparse = 0;
	my $trouble = 0;
	foreach my $x (split(/^/,$tmp)) {
		my $sxne = '';
			my $info = '';
#		$cas->log(1,"transition $x");
		if ($x =~ s{\[\|(.*?)\|\]\s+}{}g) {
			$info = $1;
			##			$info = decode("iso-8859-1",$info);
			my ($left,$comment,$token,$right) = ($x =~ /^(\d+)\s+\{(.*?)\}\s+(\S+)\s+(\d+)/);
			my ($name,$data,$url) = split(/_{3,4}/,$info);
			if ($data !~ /UNKNOWN_PERSON/) {
				##$cas->log(5,"data handling: $data");
				##$name =~ s/_\(\d+\)$//o;
				$name =~ s/_/ /og;
				my $href;
				my $htype;
				if ($url =~ s{<a_href="(.*?)">\s*(.*?)\s*</a>\s*$}{}o) {
					$href = $1;
					$htype = $2;
					$htype =~ s/_/ /og;
					## $cas->log(5,"data htype=$htype href=$href");
				}
				$data =~ s/_/ /og;
#				$cas->log(1,"send data '$name' to aleda");
				$data ||= $cas->{server}->aleda($name);
				$left--;
				$right--;
				my $data_key = "data_".($sid+1)."_${left}_${right}_$token";
				my $xml = XML::Twig::Elt->new(data => {
					key => $data_key,
					name => $name,
				}, $data);
				$sxne = $name;
				if ($href && $htype) {$xml->set_att(
					htype => $htype, 
					href => $href,
				); }
				$xml->paste( last_child => $database );
			}
		}
		$dag .= $x;
		if (1) {
		  ## EVDLC: beware of the distinction between token and wordform (as specified in MAF)
		  ## a word form may be attached to several tokens and a tokens (such as auquel) may correspond
		  ## to several wordforms
		  ## in the CAS we store tokens (and not wordform)
		  ## wordform are added at the level of the parses (as cluster+node)
		  ## if necessary, wordforms could be added in the CAS
		  ## but in that case, if would be nice to follow the MAF specif
		  my @toks = ();
		  my @tokforms = ();
		  while ($x =~ m{<F\s+id="(.+?)">\s*(.*?)\s*</F>}go) {
		    my ($tid,$form) = ($1,$2);
		    $form =~ s/^\\{2,4}([+?*-])/$1/o;
		    $form =~ s/^\\([%?()\|])/$1/o;
		    $form =~ s/\\{2}/\\/og;
		    $form =~ s/_ACC_O/\{/o;
		    $form =~ s/_ACC_F/\}/o;
		    $form =~ s/^_-/-/o;
		    ##	$cas->log(4,"cache token $tid $form");
		    $cache->{$tid} ||= XML::Twig::Elt->new(
							   'token' => {
								       id => $tid,
								       ## form => decode("latin1",$form)
								       form => $form
								       # form => decode("utf-8",$form),
								       # sxform as defined by Damien would be a wordform
								       # sxform => decode("utf-8",$sxform),
								       # sxne => decode("utf-8",$sxne),
								      });
				$stokens{$tid} = $form;
				push(@toks,$tid);
				push(@tokforms,$form);
			}
			## prepare the possible registration of word forms
			## but a wordform is rather to be handled using the parse results
			# if ($x =~ m{</F>\s*\}\s+(\S+)}) {
			#   my $wordform = $1;
			#   ## need to generate some id for the wordform
			#   my $wf = XML::Twig::Elt->new(
			# 				 'wordform' => {
			# 						id => $wid,
			# 						form => $wordform,
			# 						tokens => \@toks
			# 					       }
			# 				)
			#     $wf->paste( last_child => $wf_container );
			# }

			## Solution propos�e par Damien
			if ($x =~ m{</F>\s*\}\s+(\S+)\s+(\S+)}) {
				my $wf = $1;
				#my $infos = $2;
				my $wfid = join('_', @toks);
				my $tokens = join(' ', @tokforms);
				# $cas->log(2,"form $wf $wfid $tokens");
				$cacheforms->{$wfid} ||= XML::Twig::Elt->new(
					'wordform' => {
							id => $wfid,
							sid => $sid,
							form => $wf,
							infos => $info,
							tokens => $tokens
						}
					);
			}

		}
		if ($x =~ /##\s*DAG END/) {
			$cas->{sentences}{$sid}{udag} = $dag;
			my @tokens = ();
			foreach my $tid (sort { (split('F', $a))[1] <=> (split('F', $b))[1] } keys %stokens) {
			# map {$_->[1]} join {$a->[0] <=> $b->[0]} map {[(split('F',$_))[1],$_]} keys %tokens
				push(@tokens, $stokens{$tid})
			}
			$cas->{sentences}{$sid}{tokens} = join(' ', @tokens);
			%stokens=();
			$sid++;
			$dag='';
		}
	}
}

sub split_text {
	my $text = shift;
	## may be necessary to break sentence on some special markers
	## A better solution would be to transform them into EPSILON
	return split(/(=\(.+?\)=)/,$text);
}

1;

__END__

=head1 NAME

NewProcess::SxPipe - Preprocess NewsXML document with SxPipe

=head1 SYNOPSIS

	use NewProcess::SxPipe;

	NewsProcess::SxPipe::process($cas);

=head1 DESCRIPTION

NewProcess::SxPipe - Preprocess NewsXML document with SxPipe

Tokenize document and prepare stuff for EDyLex (tokens not recognized as errors, neologisms, lexical inventions, etc.)

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
Damien Nouvel <damien.nouvel@inria.fr>
Beno�t Sagot <benoit.sagot@inria.fr>

=cut
