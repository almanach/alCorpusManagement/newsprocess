package NewsProcess::Melt;

use strict;
use XML::Twig;
use IO::String;
use Encode;
use IPC::Run;
##use utf8;
use IPC::Open2;

sub process {

	my $class = shift;
	my $cas = shift;

	my $document = $cas->document;	# the NewML document
	my $root = $cas->root;

	$cas->log(1,"Executing MElt over sentences\n");
	my @udags = ();
	my $melt_in = '';
	foreach my $sid (keys $cas->{sentences}) {
		my $udag = $cas->{sentences}{$sid}{udag};
		my @tokens = ();
		my @token_ids = ();
		foreach my $utok (split(/^/,$udag)){
			if($utok =~ /^##DAG BEGIN/){
				@tokens = ();
			}elsif($utok =~ /^##DAG END/){
				for(1..$#tokens){
					if($_ > 1){ $melt_in .= " "; }
					if(@token_ids[$_] && @tokens[$_]){ $melt_in .= "{".@token_ids[$_]."} ".@tokens[$_]; }
				}
				$melt_in .= "\n";
			}elsif($utok =~ /^\d+\s+_PAR_BOUND\s+\d+$/){
				$tokens[1] = "_PAR_BOUND";
			}elsif($utok =~ /^\d+\s+_End_Of_Sentence_\s+\d+$/){
			}elsif($utok =~ /^\d+\s+{(<.*>)}\s+[^ ]+(?:\s+\[\|.*\|\])?\s+\d+$/){
				my $token_forms = $1;
				while($token_forms =~ s/^\s*<F id=\"(E\d+F(\d+))\">(.*?)<\/F>\s*//){
					my $full_tok_id = $1;
					my $tok_id = $2;
					my $tok = $3;
					$tok =~ s/\\([()\*\+\?])/\1/g;
					@tokens[$tok_id] = $tok;
					@token_ids[$tok_id] = $full_tok_id;
				}
				$cas->log(1,"*** Pb melt: Invalid token list in input UDAG ($_)\n") unless $token_forms eq "";
			}else{
				$cas->log(1,"*** Pb melt: Invalid line in input UDAG ($_)\n");
			}
		}
	}
	#$cas->log(3,"Executing MElt over content $melt_in\n");

	my $tmp = ' ' x 20000; # preallocate to be faster
	$tmp = '';

	my $parse_stats = $cas->{parse_stats} ||= {
		start => $cas->elapsed,
		disamb => 0,
		parse => 0,
		forest => 0
	};

	my $melt_cmd = $cas->config->{melt}{cmd};
	$cas->log(1,"MElt cmd=$melt_cmd");

	while (1) {
		eval {
			my $pid = open2(*OUT,*IN,${melt_cmd});
			binmode(IN, ":utf8");
			binmode(OUT, ":utf8");
			print IN $melt_in;
			close(IN);
			$tmp .= $_ while (<OUT>);
			close(OUT);
			waitpid $pid, 0;
		};
		if ($@) {
			$cas->log(1,"*** Pb melt: $@");
		} else {
			last;
		}
	}
	$parse_stats->{melt} = $cas->elapsed;
	#$cas->log(3, "MElt output: $tmp");
	
	while($tmp =~ s/([^{]*\{[^{]*)//){
		my $token = $1;
		#$cas->log(3, "MElt token $token");
		if($token =~ m/\{([^}]*)\}\s+[^\/]*\/([^\/ ]*)/){
			my $wfid = $1;
			my $cat = $2;
			if($cas->{forms}{$wfid}){
				#$cas->log(3, "Assign MElt form $wfid pos $cat");
				$cas->{forms}{$wfid}->set_att('pos', $cat);
			}
		}
	}
}

1;

__END__

=head1 NAME

NewProcess::Melt - Process sxpipe output with Melt

=head1 SYNOPSIS

	use NewProcess::Melt;

	NewsProcess::Melt::process($cas);

=head1 DESCRIPTION

NewProcess::Melt - Preprocess NewsXML document with Melt

Tokenize document and prepare stuff for EDyLex (tokens not recognized as errors, neologisms, lexical inventions, etc.)

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>
Damien Nouvel <damien.nouvel@inria.fr>
Beno�t Sagot <benoit.sagot@inria.fr>

=cut
