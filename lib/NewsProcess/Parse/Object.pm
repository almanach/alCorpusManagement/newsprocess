package NewsProcess::Parse::Object;
use Moose;

has  deriv => ( is => 'ro', 
		isa => 'Str',
	      );
has 'id' => (is => 'ro', isa => 'Str');
has 'xml' => (is => 'ro', isa => 'XML::Twig::Elt');
has 'cas' => ( is => 'ro', 
	       isa => 'NewsProcess::CAS',
	       weak_ref => 1
	     );

sub apply {
  my $self = shift;
  my $query = shift;
  $query->($self);
}

sub debug {
  my $self = shift;
  $self->cas->log(2,"Object ".$self->sprint);
  $self;
}

sub sid2pos {
  my $self = shift;
  $self->cas->sid2pos($self->xml);
}

1;
