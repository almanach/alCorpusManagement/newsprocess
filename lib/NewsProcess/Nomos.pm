package NewsProcess::Nomos;

use strict;
use XML::Twig;
use IO::String;
use IPC::Run;
use IPC::Open2;

sub process {

	my $class = shift;
	my $cas = shift;

	$cas->log(2,"Starting Nomos process");

	my $newsml = $cas->newsml->copy;
	#my $newsmlout = $newsml->sprint;
	#$cas->log(2,"newsml".$newsml.$newsmlout);
	my $dag_pre = $cas->{dag_pre};
	#$cas->log(2,"udag".$dag_pre);

	my $parse_stats = $cas->{parse_stats} ||= {
		start => $cas->elapsed,
		disamb => 0,
		parse => 0,
		forest => 0
	};

	# Preprocess for nomos
	my $tmp = ' ' x 20000; # preallocate to be faster
	$tmp = '';
	my $nomos_cmdpre = $cas->config->{nomos}{cmdpre};
	$cas->log(1,"nomos cmdpre=$nomos_cmdpre");
	while (1) {
		eval {
			my $pid = open2(*OUT,*IN,"${nomos_cmdpre}");
			print IN $dag_pre;
			close(IN);
			$tmp .= $_ while (<OUT>);
			close(OUT);
			waitpid $pid, 0;
		};
		if ($@) {
			$cas->log(1,"*** Pb sxpipe (pre): $@");
		} else {
			last;
		}
	}
	$tmp = '<dagpre>'.$tmp.'</dagpre>';
	$tmp =~ s/> +</></g;
	#$cas->log(2,"nomos pre output: ".$tmp);

	# Replace body.content in newsml
	foreach my $body ($newsml->descendants('body.content')) {
		$body->set_inner_xml($tmp);
	}
	#$newsml->set_pretty_print('none');
	my $newsmlnomos = $newsml->sprint;
	$cas->log(2,"newsml nomos input".$newsmlnomos);

	$parse_stats->{nomos} = $cas->elapsed;

=fromedylex
	my $edylex_tickets = $cas->add_container('edylex');

	$cas->log(2,"  search unknown words");
	my %incforms = ();
	foreach my $wfid (keys $cas->{forms}) {
		my $wf = $cas->{forms}{$wfid};
		my $sid = $wf->att('sid');
		my $form = $wf->att('form');
		my $infos = $wf->att('infos');
		my $pos = $wf->att('pos');
		my $inctype = my $toks = undef;
		if ($form =~ m{^_(INC|NEO|LEX|ASSOC|PREFIX|SUFFIX) *(\[\|(.*)\|\])?}){
			$inctype = $1;
			$cas->log(2,"  got inc $inctype ($infos)");
		}elsif($infos =~ m/^\[0:(.*)/){
			$infos = $1;
			$inctype = 'NE';
		}
		if($inctype){
			$toks = $wf->att('tokens');
			if(not exists($incforms{$toks})){
				$incforms{$toks} = ();
			}
			push(@{$incforms{$toks}}, [$sid, $wfid, $inctype, $form, $infos, $pos]);
		}
	}

	my $edylexid = 0;
	$cas->log(2,"  register unknown words");
	while(my ($toks, $incforms) = each(%incforms)){
		
		$edylexid++;
		$cas->log(2,"  > ticket: $toks");
		
		# Enregistrement du ticket
		my $ticket = XML::Twig::Elt->new(
		'ticket' => {
			'xmlns:xsi' => 'http://www.w3.org/2001/XMLSchema-instance',
			'xsi:noNamespaceSchemaLocation' => 'ticketEdylexModel.xsd',
			lang => 'fr',
			id => $cas->{did}.':'.$edylexid,
		},
		(
			my $description = XML::Twig::Elt->new(
				'description',
				(
					XML::Twig::Elt->new('tokens', $toks),
					XML::Twig::Elt->new(
						'news_id' => {
							id => $cas->{did},
							date => join('-', @{$cas->{date_id}})
							#location => '???'
						}
					),
					my $occurrences = XML::Twig::Elt->new('occurrences'),
				)
			),
			my $level = XML::Twig::Elt->new(
				'level',
				(
					my $lphonetic = XML::Twig::Elt->new('level' => {type => 'phonetic'}),
					my $lmorpho = XML::Twig::Elt->new('level' => {type => 'morphology'}),
					my $lsemantics = XML::Twig::Elt->new('level' => {type => 'semantics'}),
					my $lnamedentities = XML::Twig::Elt->new('level' => {type => 'entities'}),
				)
			),
			my $relatedtickets = XML::Twig::Elt->new('relatedtickets')
		));
		
		# Enregistrement de chaque occurrence et récupération des analyses sxpipe-edylex
		my %incinfos = ();
		foreach my $incform (@{$incforms}) {
			(my $sid, my $wfid, my $inctype, my $form, my $infos, my $pos) = @{$incform};
			$cas->log(2,"    occ $wfid $inctype $form $infos $pos");
			my $formstart = undef;
			my $formend = undef;
			foreach my $tid (split('_', $wfid)){
				$cas->log(2,"    tok: $tid");
				my $token = $cas->{tokens}{$tid};
				my $tokenstart = $token->att('start');
				if(!$formstart || $tokenstart < $formstart){
					$formstart = $tokenstart;
				}
				my $tokenend = $token->att('end');
				if(!$formend || $tokenend < $formend){
					$formend = $tokenend;
				}
				my $occurrence = XML::Twig::Elt->new(
					'occurrence',
					(
						XML::Twig::Elt->new('offset' => {begin => $formstart, end => $formend}),
						XML::Twig::Elt->new('sentence', $cas->{sentences}{$sid}{tokens})
					)
				);
				$occurrence->paste(last_child => $occurrences);
			}
			my @infos_edylex = split(';', $infos);
			if($pos){
				my @neos_selected = ();
				for my $neo (@infos_edylex) {
					$cas->log(2,"    test melt-compatible $neo");
					# Compare EDyLex cat with MElt cat
					my $neopos = uc((split(/_/, (split(/:/, $neo))[3]))[0]);
					if($neopos eq 'PREL'){ $neopos = 'PRO'; }
					if($pos eq 'P'){ $pos = 'PREP'; }
					#print "TEST $neopos $cat\n";
					if($neopos =~ /^$pos/ or $pos =~ /^$neopos/){
						#print "MATCH $neopos $cat\n";
						push(@neos_selected, $neo);
					}
				}
				if($inctype ne 'INC'){
					# For all but INC(Ana), if not compatible analysis, keep all analysis
					if(!@neos_selected){
						#print "KEEPALL@neos_selected\n";
						@neos_selected = @infos_edylex;
					}
					if($inctype eq 'ASSOC'){
						# For associations, select analyses with : 1/ the greater number of common parts 2/ the longest prefix of common parts
						my $common_parts = 0;
						my $common_prefix = 0;
						my @neos_selected_assoc = ();
						my @forms_parts = split('-', $form);
						for my $neo_selected (@neos_selected){
							my @neo_selected_parts = split('-', (split(':', $neo_selected))[1]);
							#print "PARTS@neo_selected_parts\n";
							if(@forms_parts == @neo_selected_parts){
								#print "TESTPARTS @forms_parts / @neo_selected_parts\n";
								my $neo_common_parts = 0;
								my $neo_common_prefix = 0;
								for my $i (0 .. $#forms_parts) {
									if(@forms_parts[$i] eq @neo_selected_parts[$i]){
										$neo_common_parts++;
									}
									if($neo_common_parts == $i + 1){
										$neo_common_prefix = $neo_common_parts;
									}
								}
								if($neo_common_parts > $common_parts or $neo_common_parts == $common_parts and $neo_common_prefix >= $common_prefix){
									if($neo_common_parts > $common_parts or $neo_common_prefix > $common_prefix){
										$common_parts = $neo_common_parts;
										$common_prefix = $neo_common_prefix;
										@neos_selected_assoc = ();
									}
									push(@neos_selected_assoc, $neo_selected);
									#print "DOMIN $common_parts / $common_prefix / @neos_selected_assoc\n";
								}
							}
						}
						if(@neos_selected_assoc){
							#print "REPLACE @neos_selected / @neos_selected_assoc\n";
							@neos_selected = @neos_selected_assoc;
						}
					}
				}
				foreach my $info_edylex (@neos_selected) {
					my @info_edylex_parts = split(':', $info_edylex);
					if(scalar(@info_edylex_parts) == 4){
						$incinfos{$info_edylex} = 1;
					}
				}
				my $infos_edylex_nb = keys @infos_edylex;
				my $incinfos_nb = keys %incinfos;
				$cas->log(2,"    selected melt-compatible ($pos) and unique analysis as tickets $incinfos_nb / $infos_edylex_nb");
			}
		}
		
		# Enregistrement des analyses en sortie de sxpipe-edylex
		my $analysis_count = 0;
		if(scalar(keys %incinfos)){
			$cas->log(2,"    sxpipe edylex output");
			my $morphosxpipe = XML::Twig::Elt->new('module_output' => {module_name => 'sxpipe_edylex'});
			my $date = strftime "%Y-%m-%dT%H:%M:%S", localtime;
			foreach my $info_edylex (keys %incinfos) {
				$cas->log(2,"     - add analysis $info_edylex");
				my @info_edylex_parts = split(':', $info_edylex);
				$analysis_count += 1;
				my $sxpipeanalysis = XML::Twig::Elt->new(
					'hypothesis' => {date => $date, id => 'sxpipe_edylex_'.$analysis_count},
					(
						XML::Twig::Elt->new('content', XML::Twig::Elt->new('guessItem' => {
							lemma => $info_edylex_parts[1],
							paradigm => $info_edylex_parts[2],
							pos => $info_edylex_parts[3]
						})),
						XML::Twig::Elt->new('explanation', XML::Twig::Elt->new('feature' => {
							name => 'sxpipe_module',
							value => $info_edylex_parts[0]
						}))
					)
				);
				$sxpipeanalysis->paste(last_child => $morphosxpipe);
			}
			$morphosxpipe->paste(last_child => $lmorpho);
		}
		
		# Interrogation du guesser syllabs
		my $curl = WWW::Curl::Easy->new;
		$curl->setopt(CURLOPT_HEADER,1);
		$curl->setopt(CURLOPT_POST, 1);
		$curl->setopt(CURLOPT_TIMEOUT, 300);
		$curl->setopt(CURLOPT_URL, 'http://api.greenhouse.syllabs.com/guess');
		$curl->setopt(CURLOPT_HTTPHEADER, ['Content-Type:application/xml', 'Accept: application/xml']);
		$curl->setopt(CURLOPT_POSTFIELDS, '<?xml version="1.0" encoding="UTF-8" ?><tickets><ticket lang="fr"><description><tokens>'.$toks.'</tokens></description></ticket></tickets>');
		my $syllabse_response;
		$curl->setopt(CURLOPT_WRITEDATA, \$syllabse_response);
		$cas->log(2,"    send syllabs request");
		my $retcode = $curl->perform();
		if ($retcode == 0) {
			my $syllabs_xml = XML::Twig->new();
			$syllabs_xml->parse(HTTP::Response->parse($syllabse_response)->decoded_content);
			my @syllabs_morphos = ($syllabs_xml->get_xpath('/tickets/ticket/level[@type=\'morphology\']'))[0];
			if(@syllabs_morphos){
				my $syllabs_morphos = @syllabs_morphos[0];
				my @syllabs_guess = $syllabs_morphos->get_xpath('./module_output[@module_name="syllabs_guesser"]');
				if(@syllabs_guess){
					my $syllabs_guess = @syllabs_guess[0];
					my @syllabs_hyps = $syllabs_guess->get_xpath('./hypothesis');
					if (@syllabs_hyps){
						$cas->log(2,"     - got syllabs response at morphological level");
						$syllabs_guess->copy()->paste(last_child => $lmorpho);
						# Log des infos trouvées par Syllabs
						foreach my $syllabs_hyp (@syllabs_hyps) {
							my $syllabs_hyp_lemma = ($syllabs_hyp->get_xpath('./content/guessItem'))[0]->att('lemma');
							my $syllabs_hyp_para = ($syllabs_hyp->get_xpath('./content/guessItem'))[0]->att('paradigm');
							my $syllabs_hyp_conf = ($syllabs_hyp->get_xpath('./explanation/feature[@name="confidence"]'))[0]->att('value');
							$cas->log(2,"     - syllabs hypothesis: $syllabs_hyp_lemma $syllabs_hyp_para $syllabs_hyp_conf");
						}
					}else{
						$cas->log(2,"     - no syllabs guess");
					}
				}
			}
		} else {
			$cas->log(2,"     - error retrieving syllabs response : $retcode ".$curl->strerror($retcode)." ".$curl->errbuf."\n");
		}
		
		# Ajout du ticket
		$ticket->paste(last_child => $edylex_tickets);
	}
=cut

}

1;

__END__

=head1 NAME

NewProcess::Edylex - Extract edylex tickets from news using sxPipe

=head1 SYNOPSIS

  use NewProcess::Edylex;

  NewsProcess::Edylex::process($xml);

=head1 DESCRIPTION

NewProcess::Edylex - Extract edylex tickets from news using sxPipe

The verbatim container has the following form
<edylex-tickets>
  <edylex id="edylex_<id>" where="elt_id" start="0" end="u">To be defined...</edylex>
  ...
</edylex-tickets>

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Damien Nouvel <damien.nouvel@inria.fr>
Benoît Sagot <benoit.sagot@inria.fr>

=cut
