package NewsProcess::Entities;

use strict;
use XML::Twig;
use Geo::GeoNames;
use String::Approx qw/aindex amatch/;
use NewsProcess::Query::Compile;
use Encode;
use Text::WagnerFischer qw(distance);

our %en_map = (
	       _DATE_artf => 'date',
	       _DATE_arto => 'date',
	       _ADRESSE => 'adresse',
	       _URL => 'url',
	       _DIMENSION => 'dimension',
	       _EMAIL => 'email',
	       _HEURE => 'heure',
	       _NUM => 'number',
	       _NUMBER => 'number',
	       _TEL => 'tel',
	       _LOCATION => 'location',
	       _PERSON => 'person',
	       _PERSON_m => 'person',
	       _PERSON_f => 'person',
	       _COMPANY => 'company',
	       _ORGANIZATION => 'organization',
	       _PRODUCT => 'product',
	       _WORK => 'work',
	      );

sub process {
  my $class = shift;
  my $cas = shift;
  my $xml = $cas->add_container('entities');
#  $cas->log(2,"processing entities");
  my $geousername = $cas->config->{geoname}{username} || 'newsprocess';
  my $entities = { xml => $xml,
		   cache => {},
		   geo => Geo::GeoNames->new( username => $geousername ), # should be configurable
		   data => $cas->data('entities'),
		   countries => {},
		   sxpipe_data => {},
		 };

  if (my $database = $cas->root->first_child('database')) {
    foreach my $data ($database->children('data')) {
#      $cas->log(2,"registering data key ".$data->att('key'));
      $entities->{sxpipe_data}{$data->att('key')} = $data;
    }
  }

  ## First pass: collect occurences
  ## and very simple entity classification
  foreach my $dep ($cas->root->first_child('parses')->children('dependencies')) {
    my $clusters = {};
    foreach my $cluster ($dep->children('cluster')) {
      $clusters->{$cluster->att('id')} = $cluster;
      if (0) {
	my $token = $cluster->att('token');
	if (my $type=$en_map{$token}) {
	  cluster2EN($cas,$entities,$cluster,$type);
	}
      }
    }
    foreach my $node ($dep->children('node')) {
      my $cluster = $clusters->{$node->att('cluster')};
      next if ($cluster->att('#entity_occ'));
      my $form = $node->att('form');
##      next if ($form 
      next if ($form eq "_SPECWORD");
      my $type=$en_map{$form};
      $type = 'np' if (!$type && ($node->att('cat') eq 'np'));
#      $cas->log(1,"cluster type=$type");
      cluster2EN($cas,$entities,$cluster,$type,$form) if ($type);
    }
  }
  ## Second pass: better classification
  my @keys = 
    sort 
      {(@{$entities->{cache}{$a}{v}} <=> @{$entities->{cache}{$b}{v}})
	 || length($entities->{cache}{$a}{lex}) <=> length($entities->{cache}{$b}{lex})
       } 
	keys %{$entities->{cache}};
  ## sort by increasing number of component in entities
  while (@keys) {
    my $clex = shift @keys;
    my $candidate = $entities->{cache}{$clex};
    my $type = $candidate->{type};
    next if ($type eq 'number'); # numbers should be equals
    next if (($type eq 'date') 
	     && ($clex =~ /\d+/)); # dates should be equals
    my $best;
    my $max=0;
    my $data = $candidate->{xml}->first_child('data');
##    print STDERR "lex=$clex type=$type candidate=$candidate\n";
    foreach my $lex (@keys) {
      my $en = $entities->{cache}{$lex};
##      next if (($type ne 'np') && ($en->{type} ne 'np') && ($en->{type} ne $type ));
##      next if ($data && $en->{xml}->first_child('data'));
      my @vc = @{$candidate->{v}};
      my @v = @{$en->{v}};
      my $n = 0;
      my $lastmatch = 0;
      my $included = 1;
      while (@vc && @v) {
	if (string_cmp($vc[0],$v[0])) {
	  $n += length($v[0]);
	  shift @vc;
	  shift @v;
	  $lastmatch = 1 unless (@vc);
	} elsif (grep {string_cmp($vc[0],$_)} @v) {
	  my $x = shift @vc;
	  shift @v;
	  while (@v) {
	    my $y = shift @v;
	    next unless (string_cmp($x,$y));
	    $n += length($x);
	    last;
	  }
	  $lastmatch = 1 unless (@vc);
	} elsif (grep {string_cmp($v[0],$_)} @vc) {
	  $included = 0;
	  my $x = shift @v;
	  shift @vc;
	  while (@vc) {
	    my $y = shift @vc;
	    next unless (string_cmp($x,$y));
	    $n += length($x);
	    last;
	  }
	  $lastmatch = 1 unless (@vc);
	} else {
	  $included = 0;
	  shift @vc;
	  shift @v;
	}
      }
      if ($lastmatch && ($n > $max)) {
	if ($included 
	    || ($en eq 'np') 
	    || ($en->{type} eq 'np')
	    || ( ($en->{type} eq $type)
		 && ( !$data 
		      || !$en->{xml}->first_child('data')
		    )
	       )
	   ) {
	  ## we don't unite two entities together if they both have a data field
	  ## except if the candidate is fully included in $en
	  ## such as Chirac included in 'Jacques Chirac'
	  $best = $lex;
	  $max = $n;
	}
      }
    }
    if (defined $best) {
      my $xml = $entities->{cache}{$best}{xml};
##      print STDERR "attach $clex to $best\n";
      foreach my $occ ($candidate->{xml}->children('occ')) {
	$occ->cut;
	$occ->paste(last_child => $xml);
      }
      if (!$xml->first_child('data') && $candidate->{xml}->first_child('data')) {
	$candidate->{xml}->first_child('data')->cut->paste(first_child => $xml);
      }
      delete $entities->{cache}{$clex};
    }
  }
  ## Pass 3: remaining entities are attached to their container
  foreach my $lex (keys %{$entities->{cache}}) {
    my $en = $entities->{cache}{$lex};
    my $entity = $en->{xml};
    $entity->add_id;
    $cas->register($entity);
    ## better types for entities
    my %types = ();
    foreach my $occ ($entity->children('occ')) {
      $types{$occ->att('type')}++;
    }
    my @types = sort {$types{$b} <=> $types{$a}} keys %types;
    my $type = $en->{type};
    my $best = $types[0];
    if ($best ne 'np' && ($best ne $type)) {
      $cas->log(2,"for '$lex' changed entity type from $type to $best");
      $entity->set_att(type => $best);
      $en->{type} = $type = $best;
    }
    if (0) {
      foreach my $occ ($entity->children('occ')) {
	next unless ($occ->att('type') eq $type);
	if (my $data = $occ->first_child('data')) {
	  $data->copy->paste( first_child => $entity );
	  last;
	}
      }
    }
    ## check in data and register
    if (grep {$_ eq $en->{type}} qw/np person location/) {
      my $data = $entities->{data}{$lex} 
	||= { variants => { $entity->att('name') => 1 }
	    };
      foreach my $occ ($entity->children('occ')) {
	$data->{variants}{$occ->att('alias')} = 1;
      }
      $data->{documents}{$cas->{did}} = 1;
## deactivated because of pb with the new version of geoname which require a login name
##      ($en->{type} ne 'person') and try_geoname($entities,$lex,$entity);
    }
    $entity->paste( last_child => $entities->{xml} );
  }
  ## Pass 3+: check some correlation between entities
  foreach my $country (keys %{$entities->{countries}}) {
    my @geo = @{$entities->{countries}{$country} || []};
    my $n = 0;
    $n += $_->[1] foreach (@geo);
    foreach my $geo (@geo) {
      $geo->[0]->set_att(priority => $n+$geo->[1]) ;
      if ($n > 8) {
	$entities->{data}{countries}{$country} ||= {};
	$entities->{data}{countries}{$country}{$geo->[0]->parent->att('name')} = 1;
      }
    }
  }
  ## Pass4: finding apposition
  foreach my $edge ($cas->find_edges(dpath is_subst is_N2app)) {
    my $end = $edge->target;
    my $start = $edge->source->parent;
    if (my $entity = $start->entity) {
      next unless ($entity->att('type') eq 'person');
      my $pos = $end->xoffset;
      $pos->extrude($start->offset);
      my $appos = XML::Twig::Elt->new( 'apposition'
				       => { node => $end->id },
				       $pos->clean_txt
				     )->set_pos($pos);
      if (my $indirect = $end->indirect_entity) {
	$appos->set_att( indirect_entity => $indirect->att('id'));
      }
      $appos->paste(last_child => $entity);
    } elsif (my $entity = $end->entity) {
      next unless ($entity->att('type') eq 'person');
      my $pos = $start->xoffset;
      $pos->extrude($end->offset);
      my $appos = XML::Twig::Elt->new( 'apposition'
					      => { node => $start->id },
					      $pos->clean_txt
					    )->set_pos($pos);
      if (my $indirect = $start->indirect_entity) {
	$appos->set_att( indirect_entity => $indirect->att('id'));
      }
      $appos->paste(last_child => $entity);
    }
  }
  foreach my $edge ($cas->find_edges(dpath is_lexical is_np)) {
    my $np = $edge->target;
    my $function = $edge->source;
    if (my $entity = $np->entity) {
      my $pos = $function->xoffset;
      my $title = $function->lemma;
      my $appos = XML::Twig::Elt->new( 'apposition'
					      => { node => $function->id,
						   title => $title
						 },
					      $pos->clean_txt
					    )->set_pos($pos);
      if (my $indirect = $function->indirect_entity) {
	$appos->set_att( indirect_entity => $indirect->att('id'));
      }
      $appos->paste(last_child => $entity);
    }
  }
#  foreach my $k (keys %{$entities->{data}}) {
#    $cas->log(4,"data registered entity $k");
#  }
  $cas->{server}->data_set(entities => $entities->{data});
}

sub string_cmp {
  my ($s1,$s2) = @_;
  my $distance = 2*distance($s1,$s2) / (length($s1)+length($s2));
  return ($distance < .2) ? 1 : 0;
}

sub cluster2EN {
  my $cas = shift;
  my $entities = shift;
  my $cluster = shift;
  my $type = shift;
  my $form = shift;
  my $lex = $cluster->att('lex');
  my $xlex = token2txt($lex);
  $xlex =~ s/^(l|d|c|qu|Qu|C)'//;
  return if (lc($xlex) eq $xlex);
  return if (length($xlex) eq 1);
  my $stdlex = lc($xlex);
  my $ids = token2ids($lex);

  my $cid = $cluster->att('id');
  my $left = $cluster->att('left');
  my $right = $cluster->att('right');
  my $token = $cluster->att('token');
  my ($sid) = ($cid =~ /^E(\d+)/);
  my $data_key = "data_${sid}_${left}_${right}_${form}";

  my $occ = XML::Twig::Elt->new( occ => {
					 tokens => join(' ',@$ids),
					 alias => $xlex,
					 type => $type,
					 key => $data_key
					}
			       );
  ##  $cas->log(2,"searching data key $data_key");
  if (my $data = $entities->{sxpipe_data}{$data_key}) {
    $data->copy->paste( first_child => $occ );
    # use canonical info when available
    if (my $name = $data->att('name')) {
      $xlex = $name;
      $xlex =~ s/_/ /og;	# done in Parse (but to be sure)
      $data->set_att(name => $xlex);
      ## remove trailing (..) in names
      $xlex =~ s/\s+\(\d+\)$//o;
      $stdlex = lc($xlex);
    }
  }
  ## private attribute: no output (see XML::Twig)
  $cluster->set_att('#entity_occ' => $occ);
  if (my $pos = $cas->cluster2pos($cluster)) {
    $occ->set_pos($pos);
  }
  if ($type eq 'person') {
    if ($cluster->att('token') =~ /PERSON_([mf])/) {
      $occ->set_att(gender => $1);
    }
  }
  my $entity;
  if ($entity = $entities->{cache}{$stdlex}{xml}) {
    $occ->paste(last_child => $entity);
  } else {
    $entity = XML::Twig::Elt->new( 
				     entity => { name => $xlex, 
						 type => $type
					       },
					    $occ
				    );
    my $x = $entities->{cache}{$stdlex} = { xml => $entity,
					    v => [ split(/\s+/,$stdlex) ],
					    type => $type,
					    lex => $xlex
					  };
    ##    print STDERR "add lex=$stdlex type=$x->{type} candidate=$x\n";
  }
  if (my $gender = $occ->att('gender')) {
    if (!$entity->att('gender')) {
      $entity->set_att(gender => $gender);
    }
  }
  if (!$entity->first_child('data') && $occ->first_child('data')) {
    $occ->first_child('data')->copy->paste( first_child => $entity );
  }
  return $entity;
}

sub token2txt {
  my $tokens = shift;
  my @tokens = ();
  ## avoid sxpipe duplication within tokens
  foreach my $t (split(/\s+/,$tokens)) {
    push(@tokens,$t) unless (grep {$t eq $_} @tokens);
  }
  my @txt = map {/^E\d+F\d+\|(.*?)$/ && $1} @tokens;
  return join(' ',@txt);
}

sub token2ids {
  my $tokens = shift;
  my @tokens = ();
  foreach my $t (split(/\s+/,$tokens)) {
    push(@tokens,$t) unless (grep {$t eq $_} @tokens);
  }
  my @ids = map {/^(E\d+F\d+)/ && $1} @tokens;
  return [@ids];
}

sub try_geoname {
  my $entities = shift;
  my $en = shift;
  my $entity = shift;
  eval {
    my $res = $entities->{data}{$en}{geoname}
      ||=  $entities->{geo}->search( name => $en, maxRows => 3, lang => 'fr' );
    if (@$res) {
      $entity->set_att(geoname => 1);
      foreach my $info (@$res) {
	my $geo = XML::Twig::Elt->new( geoname => $info );
	$geo->paste(last_child => $entity);
	my $fcl = $info->{'fcl'};
	my $fcode = $info->{'fcode'};
	my $priority = 0;
	if (grep {$fcode eq $_} qw/PCLI ADM1/) {	
	  # countries or major division in country
	  $priority = 10;
	} elsif ($fcode eq "PPLC") { # capitals
	  $priority = 5;
	} elsif ($fcode eq "PPLA") { # major cities
	  $priority = 3;
	} elsif($fcode =~ /^PPL/) { # populated place
	  $priority = 1;
	}
	if  (my $country = $info->{'countryCode'}) {
	  my $info = $entities->{countries}{$country} ||= [];
	  $priority += 4 if ($country eq 'FR'); # favor France
	  push(@$info,[$geo,$priority]);
	}
      }
    } else {
      $entity->set_att(geoname => 0);
    }
  };
}

sub approx {
  my $entities = shift;
  my $candidate = shift;
  foreach my $lex (keys %{$entities->{cache}}) {
    next unless (aindex($lex,$candidate) != -1) 
      || (aindex($candidate,$lex) != -1);
    return $entities->{cache}{$lex}{xml};
  }
}

sub approx2 {
  my $entities = shift;
  my $candidate = shift;
  my $v2 = {map {$_ => 1} split(/\s+/,$candidate)};
  my %found = ();
  foreach my $lex (keys %{$entities->{cache}}) {
    my $e = $entities->{cache}{$lex};
    my $v = $e->{v};
    my $number = 0;
    my $n1 = 0;
    my $r1 = 0;
    foreach my $k (keys %$v2) {
      $n1 += length($k);
      $r1 += length($k) if (exists $v->{$k});
      $number = 1 if ($k =~ /^\d+$/ && !exists $v->{$k})
    }
    my $n2 = 0;
    my $r2 = 0;
    foreach my $k (keys %$v) {
      $n2 += length($k);
      $r2 += length($k)  if (exists $v2->{$k});
      $number = 1 if ($k =~ /^\d+$/ && !exists $v2->{$k})
    }
    if (!$number && ($r1 || $r2)) {
      $found{$lex} = ($r1 > $r2) ? ($r1 / $n1) : ($r2 / $n2);
    }
  }
  my $best;
  my $max = 0;
  foreach my $lex (keys %found) {
    if ($found{$lex} > $max) {
      $max = $found{$lex};
      $best = $lex;
    }
  }
  return $entities->{cache}{$best}{xml} if ($best);
}

1;

__END__

=head1 NAME

NewProcess::Entities - Named Entities extraction from DEP XML files

=head1 SYNOPSIS

  use NewProcess::Entities;

  NewsProcess::Entities::process($xml);

=head1 DESCRIPTION

=head2 EXPORT

None by default.

=head1 SEE ALSO

=head1 COPYRIGHT AND LICENSE

Copyright (C) 2009, INRIA.

This program is free software; you can redistribute it and/or modify it under
the same terms as Perl itself.

=head1 AUTHOR

Eric de la Clergerie <Eric.De_La_Clergerie@inria.fr>

=cut
