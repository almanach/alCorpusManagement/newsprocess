<?php
	$id = $_GET['idDepeche'];
	$q = "
		SELECT d.txtDepeche, d.titreDepeche, d.pathDepeche
		FROM depeche d
		WHERE d.idDepeche = $id;
	";
	include_once('./common/sql/query.php');
	$d = connex();
	$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
	if(count($res)) {
		$path = $res[0]['pathDepeche'];
		$txtDepeche = "<h1>" . $res[0]['titreDepeche'] . "</h1>" . $res[0]['txtDepeche'];
		
		$q = "
			SELECT o.idEntite, e.lblEntite, e.idTypeEntite_appartient_a, o.lblOccurrence, o.parOccurrence, o.startOccurrence, o.endOccurrence
			FROM occurrence o, entite e, typeentite te
			WHERE o.idDepeche = $id
			AND o.idEntite = e.idEntite
			AND e.idTypeEntite_appartient_a = te.idTypeEntite
			AND te.lblTypeEntite = 'np'
			;
		"; // Type 2 = np
		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		$tiden = array();
		$tocc = array();
		for($i=0;$i<count($res);++$i) {
			if(isset($tiden[$res[$i]['idEntite']])) {
				array_push($tiden[$res[$i]['idEntite']],$res[$i]);
			}
			else {
				$tiden[$res[$i]['idEntite']] = array($res[$i]);			
			}
			$tocc[$res[$i]['parOccurrence']][$res[$i]['startOccurrence']] = array("endOcc" => $res[$i]['endOccurrence'], "idEn" => $res[$i]['idEntite']);
		}
		
		ksort($tocc, SORT_NUMERIC);
		$tocc = array_reverse($tocc,true);
		
 		foreach($tocc as $k => $v) {
			ksort($tocc[$k], SORT_NUMERIC);
			$tocc[$k] = array_reverse($tocc[$k],true);
			//echo $i;
		}
		 /* 
		echo "<pre>";
		print_r($tocc);
		echo "</pre>";  *//* 
		$q = "
			SELECT c.*, ace.idEntite
			FROM citation c, depeche d, entite e, assoccitationentite ace
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite
		"; */
		$q = "
			SELECT c.*
			FROM citation c
			WHERE c.idDepeche_est_tiree_de = $id
			AND c.idCitation NOT IN (SELECT c.idCitation
			FROM citation c, depeche d, entite e, assoccitationentite ace
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite)
		";

		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		$tpar = explode("\n",$txtDepeche);
		$t = array();
		foreach($res as $k => $c) {
			//$t[$k] = array('idEntite' => $c['idEntite'], 'idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation']-1, 'txtCitation' => $c['txtCitation']);		
			array_push($t,array('idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation']-1, 'txtCitation' => $c['txtCitation']));
		}
		$q = "
			SELECT c.*, ace.idEntite
			FROM citation c, depeche d, entite e, assoccitationentite ace
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite
		";

		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		//$tpar = explode("\n",$txtDepeche);
		//$t = array();
		foreach($res as $k => $c) {
			array_push($t,array('idEntite' => $c['idEntite'], 'idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation']-1, 'txtCitation' => $c['txtCitation']));
			//$t[$k] = array('idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation']-1, 'txtCitation' => $c['txtCitation']);		
		}
		 
		
		$d = null;
		// Les num de paragraphes dans $t ont 1 de moins que dans $tocc.
		
		
		foreach($tocc as  $numPar => $tentite) {
			$par = &$tpar[$numPar-1];
			if($numPar == 1) {
				echo "<pre>";
				print_r($tocc[$numPar]);
				echo "</pre>";
				
				foreach($tocc[$numPar] as $k => $v) {
					$nK = $k + strlen('<h1>');
					$nEnd = strlen('<h1>') + (int) $v['endOcc'];
					$nId = $v['idEn'];
					
					unset($tocc[$numPar][$k]);
					$tocc[$numPar][$nK] = array('endOcc' => $nEnd, 'idEn' => $nId);
				}
				
				echo "<pre>";
				print_r($tocc[$numPar]);
				echo "</pre>";
			}
				
 			foreach($tentite as $start => $tprop) {
				
				//$par ; $start ; $end ; $idEnd ; $numPar ;
				
				$end = $tprop['endOcc'];
				$idEn = $tprop['idEn'];
				$marqueur = '<span class="occ" id="disp-' . $numPar . '-' . $start . '-' . $end . '">';
				$en = substr($par,$start,($end - $start));
				$par = substr($par,0,$start) . $marqueur . $en . '</span>' . substr($par,$end);
				foreach($t as &$quote) {					
					if($quote['parCitation'] == $numPar - 1) {
						if($quote['endCitation'] >= $end) {
							$quote['endCitation'] = strlen($marqueur . '</span>') + (int) $quote['endCitation'];;
						}
						if($quote['startCitation'] >= $end) {
							$quote['startCitation'] = strlen($marqueur . '</span>') + (int) $quote['startCitation'];
						}
					}
				}
			} 
		}
		
		function cmp_t($a,$b) {
			if($a['parCitation'] == $b['parCitation']) {
				if($a['startCitation'] == $b['startCitation']) return 0;
				return ($a['startCitation'] < $b['startCitation']) ? -1 : 1;
			}
			return ($a['startCitation'] < $b['startCitation']) ? -1 : 1;
		
		}
		usort($t,"cmp_t");
/* 		echo "<pre>";
		print_r($t);
		echo "</pre>"; */
		
		 
		foreach($t as $c) {
			if($c['parCitation'] == 0) {
				$c['startCitation'] = (int) $c['startCitation'] - strlen('<h1>');
				$c['endCitation'] = (int) $c['endCitation'] + strlen('<h1>');
			}
			$tac = &$tpar[$c['parCitation']];
			if(isset($c['idEntite'])) $tac = substr($tac,0,$c['startCitation']) . '<span id="quote-' . $c['idEntite'] . '-' . $c['idCitation'] . '" class="citation">' . substr($tac,$c['startCitation'],($c['endCitation'] - $c['startCitation'])) . "</span>" . substr($tac,$c['endCitation']);
			else $tac = substr($tac,0,$c['startCitation']) . '<span id="quote-0-' . $c['idCitation'] . '" class="citation">' . substr($tac,$c['startCitation'],($c['endCitation'] - $c['startCitation'])) . "</span>" . substr($tac,$c['endCitation']);
			
		}
		// Répércussion du <h1>
		
		 
		 
		echo '<a href="' . $path . '">Lien vers le XML Agregate</a>';
		foreach($tpar as $parag) {
			echo "<p>" . $parag . "</p>\n";
		}
	}
	else {
		echo "Erreur ! L'url que vous souhaitez consulter est mal formée ou l'entité n'existe plus.";
	}
?>

<script type="text/javascript" src="./javascript/size.js"></script>
<script type="text/javascript">

	function highlightOcc(o) {
		var id = (o.attributes.getNamedItem('id').nodeValue);
		var tid =  id.split("-");
		var occ = document.getElementById('disp-' + tid[1] + '-' + tid[2] + '-' + tid[3]);
		occ.style.backgroundColor = "#fff667";
		occ.style.fontWeight = "bold";
	}
	function unHighlightOcc(o) {
		var id = (o.attributes.getNamedItem('id').nodeValue);
		var tid =  id.split("-");
		var occ = document.getElementById('disp-' + tid[1] + '-' + tid[2] + '-' + tid[3]);
		occ.style.backgroundColor = "";	
		occ.style.fontWeight = "normal";
	}
	function highlightEn(e,isText) {
		var txt = isText | false;
		if(txt) {
			var tid = e.attributes.getNamedItem('id').nodeValue.split("-");
			en = document.getElementById('panel-' + tid[1] + '-' + tid[2] + '-' + tid[3]).parentNode;
		}
		else en=e;
		var idEn = e.attributes.getNamedItem('id').nodeValue.split("-");
		var quotes = getElementsByClassName("citation","span",document.getElementById('middle'));
		for(var i=0;i<quotes.length;++i) {
			//alert("=)");
			var tid = quotes[i].attributes.getNamedItem('id').nodeValue.split("-");
			if(idEn[1] == tid[1]) {
				//quotes[i].style.textDecoration = "underline";
			}
		}		
		en.parentNode.childNodes[0].style.backgroundColor = "#fff667";
		var content = en.parentNode.childNodes[1].childNodes;
		for(var i=0;i<content.length ;++i) {
			highlightOcc(content[i]);
			content[i].onclick = function() {unHighlight(this,true);};
		}
		e.onclick = function() {unHighlightEn(this,txt);};
	}
	function unHighlightEn(e,isText) {
		var txt = isText | false;
		if(txt) {
			var tid = e.attributes.getNamedItem('id').nodeValue.split("-");
			en = document.getElementById('panel-' + tid[1] + '-' + tid[2] + '-' + tid[3]).parentNode;
		}
		else en = e;
		var idEn = e.attributes.getNamedItem('id').nodeValue.split("-");
		var quotes = getElementsByClassName("citation","span",document.getElementById('middle'));
		for(var i=0;i<quotes.length;++i) {
			var tid = quotes[i].attributes.getNamedItem('id').nodeValue.split("-");
			if(idEn[1] == tid[1]) {
				quotes[i].style.textDecoration = "";
			}
		}	
		en.parentNode.childNodes[0].style.backgroundColor = "";
		var content = en.parentNode.childNodes[1].childNodes;
		for(var i=0;i<content.length ;++i) {
			unHighlightOcc(content[i]);	
			content[i].onclick = function() {highlight(this,true);};
		}
		e.onclick = function() {highlightEn(this,txt);};
	}


	var body = document.getElementById('body');
	var panel = document.createElement('div');
	var left = document.getElementById('left');
	
	left.style.width = "75%";
	left.setAttribute('class','seq');
	
	panel.setAttribute('id','panel');
	panel.setAttribute('class','colonne');
	body.nextSibbling;
	body.appendChild(panel);
	
	<?php
	foreach($tiden as $en) {
		?>
		var main = document.createElement('div');
		main.setAttribute('id','EN-<?php echo $en[0]['idEntite']; ?>-panel');
		
		var title = document.createElement('div');
		title.innerHTML = "+ <?php echo $en[0]['lblEntite']; ?>";
		title.setAttribute('id','EN-<?php echo $en[0]['idEntite']; ?>-title');
		title.setAttribute('class','occ');
		
		panel.appendChild(main);
		main.appendChild(title);
		// Boucle de génération des nodes du panel
		var liste = document.createElement('div');
		liste.style.display = "none";
		main.appendChild(liste);
	<?php
		// Serait plus joli : faire des structures en echo de JavaScript et boucler sur le javascript
		$str = "var tocc = new Array(";
		foreach($en as $occ) {
			$str .= "new Array('" . addslashes($occ['lblOccurrence']) . "', '" . $occ['parOccurrence'] . "-" . $occ['startOccurrence'] . "-" . $occ['endOccurrence'] . "', '" . $occ['idEntite'] . "'),";
		}
		$str = substr($str,0,-1);
		$str .= ");";
		echo $str;
	?>
		for(var i=0;i<tocc.length;++i) {
			var occ = document.createElement('div');
			occ.innerHTML = "..." + tocc[i][0];
			occ.setAttribute('id','panel-' + tocc[i][1]);
			occ.onclick = function() {highlightOcc(this);};
			liste.appendChild(occ);
			<?php
			if(isset($_GET['identite'])) {?>
				if(tocc[i][2] == (<?php echo $_GET['identite'] ?>)) {
					highlightEn(title);
					title.onclick = function () {unHighlightEn(this);};
				}
				else title.onclick =  function () {highlightEn(this);};
			<?php
			}
			else {?>
				title.onclick = function () {highlightEn(this);};
			<?php
			}
		?>
		}
	<?php
	}
	?>
	// Fin de la boucle de génération des nodes du panel
	
	// Génération des clicks dans le texte
	
	var occText = getElementsByClassName("occ","span",document.getElementById('middle'));
	for(var i=0;i<occText.length;++i) {
		occText[i].onclick = function () {
			highlightEn(this,true);
		};
	}
	
	
	
	feetHSmallCol('left', 'panel');
	feetHSmallCol('panel', 'left');
</script>