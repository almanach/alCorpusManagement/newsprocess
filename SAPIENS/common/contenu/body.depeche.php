<?php
	if(!isset($id)) $id = $_GET['idDepeche'];
	$q = "
		SELECT d.txtDepeche, d.titreDepeche, d.pathDepeche
		FROM depeche d
		WHERE d.idDepeche = $id;
	";
	include_once('./common/sql/query.php');
	$d = connex();
	$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
	if(count($res)) {
		$path = $res[0]['pathDepeche'];
		$txtDepeche = my_decode($res[0]['titreDepeche'] . $res[0]['txtDepeche']);
		
		$q = "
			SELECT o.idEntite, e.lblEntite, e.idTypeEntite_appartient_a, e.txtEntite, o.lblOccurrence, o.parOccurrence, o.startOccurrence, o.endOccurrence, te.lblTypeEntite, e.linkEntite, e.typeLinkEntite
			FROM occurrence o, entite e, typeentite te
			WHERE o.idDepeche = $id
			AND o.idEntite = e.idEntite
			AND e.idTypeEntite_appartient_a = te.idTypeEntite
			AND te.lblTypeEntite <> 'heure'
			AND te.lblTypeEntite <> 'number'
			AND te.lblTypeEntite <> 'date'
			AND te.lblTypeEntite <> 'url'
			AND te.lblTypeEntite <> 'adresse'
			AND te.lblTypeEntite <> 'np'
			;
		";
		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		$tiden = array();
		$tocc = array();
		for($i=0;$i<count($res);++$i) {
			if(isset($tiden[$res[$i]['idEntite']])) {
				array_push($tiden[$res[$i]['idEntite']],$res[$i]);
			}
			else {
				$tiden[$res[$i]['idEntite']] = array($res[$i]);			
			}
			$tocc[$res[$i]['parOccurrence']][$res[$i]['startOccurrence']]
                            = array("endOcc" => $res[$i]['endOccurrence'],
                                    "idEn" => $res[$i]['idEntite'],
                                    "type" => $res[$i]['lblTypeEntite'],
                                    'lblEntite' => $res[$i]['lblEntite'],
                                    'txtEntite' => $res[$i]['txtEntite'],
                                    'linkEntite' => $res[$i]['linkEntite'],
                                    'typeLinkEntite' => $res[$i]['typeLinkEntite']
                                    );
		}
		
		ksort($tocc, SORT_NUMERIC);
		$tocc = array_reverse($tocc,true);
		
 		foreach($tocc as $k => $v) {
			ksort($tocc[$k], SORT_NUMERIC);
			$tocc[$k] = array_reverse($tocc[$k],true);
			//echo $i;
		}
		$q = "
			SELECT c.*
			FROM citation c
			WHERE c.idDepeche_est_tiree_de = $id
			AND c.idCitation NOT IN (SELECT c.idCitation
			FROM citation c, depeche d, entite e, assoccitationentite ace
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite)
			AND c.endCitation <> 0
		";

		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		$tmp = explode("\n",$txtDepeche);
		for($i=0;$i<count($tmp);++$i) {
			$tpar[$i+1] = $tmp[$i];
		}
		$t = array();
		$tagent = array();
		foreach($res as $k => $c) {
			array_push($t,array('idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation'], 'txtCitation' => $c['txtCitation']));
		}
                    /*
		$q = "
			SELECT DISTINCT c.*, ace.idEntite, e.lblEntite, a.startAgent, a.endAgent, a.parAgent, a.txtAgent
			FROM citation c, depeche d, entite e, assoccitationentite ace, typeentite te, agent a
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite
			AND a.idCitation = c.idCitation
			AND c.endCitation <> 0
		";
                    */

		$q = "
			SELECT DISTINCT c.*, a.startAgent, a.endAgent, a.parAgent, a.txtAgent
			FROM citation c, depeche d, agent a
			WHERE c.idDepeche_est_tiree_de = d.idDepeche
			AND d.idDepeche = $id
			AND a.idCitation = c.idCitation
			AND c.endCitation <> 0
		";
                
		$res = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
                
		//$tpar = explode("\n",$txtDepeche);
		//$t = array();
		foreach($res as $k => $c) {
                    $qq = "SELECT DISTINCT ace.idEntite, e.lblEntite
                       FROM entite e, assoccitationentite ace
                       WHERE ace.idCitation = $c[idCitation]
                       AND ace.idEntite = e.idEntite
                ";

                    $tmp = array( 'idEntite' => 0,
                                  'lblEntite' => ""
                                  );
                    $res2 = makePDOQuery($d,"SELECT", $qq, array())->fetchAll();
                    foreach ($res2 as $k2 => $c2) {
                        $tmp['idEntite'] = $c2['idEntite'];
                        $tmp['lblEntite'] = $c2['lblEntite'];
                    }
                    array_push($t,array('idEntite' => $tmp['idEntite'], 'idCitation' => $c['idCitation'],'startCitation' => $c['startCitation'], 'endCitation' => $c['endCitation'], 'parCitation' =>$c['parCitation'], 'txtCitation' => $c['txtCitation'], 'lblEntite' => $tmp['lblEntite'],
                                        'txtAgent' => $c['txtAgent']
                                        ));
			
                    array_push($tagent,array('idEntite' => $tmp['idEntite'], 'lblEntite' => $tmp['lblEntite'], 'idCitation' => $c['idCitation'], 'parAgent' => $c['parAgent'], 'startAgent' => $c['startAgent'], 'endAgent' => $c['endAgent'], 'txtAgent' => $c['txtAgent']));
		}
		$q = "
			SELECT *
			FROM verbatim
			WHERE idDepeche = $id
		";		
		$verbatims = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		
		$q = "
			SELECT DISTINCT a.idAudience, a.parAudience, a.startAudience, a.endAudience, a.entityAudience, a.idCitation, ace.idEntite, e.lblEntite
			FROM audience a, citation c, entite e, assoccitationentite ace, typeentite te
			WHERE a.idCitation = c.idCitation
			AND c.idDepeche_est_tiree_de = $id
			AND c.idCitation = ace.idCitation
			AND ace.idEntite = e.idEntite
			AND a.idCitation = c.idCitation
			AND c.endCitation <> 0
		";
		$taudience = makePDOQuery($d,"SELECT", $q, array())->fetchAll();
		$q = "
			SELECT DISTINCT a.idAudience, a.parAudience, a.startAudience, a.endAudience, a.entityAudience, a.idCitation
			FROM audience a, citation c
			WHERE a.idCitation = c.idCitation
			AND c.idDepeche_est_tiree_de = $id
		";
		$taudience = array_merge($taudience, makePDOQuery($d,"SELECT", $q, array())->fetchAll());

                    // Predicate table
                
                $q = "
			SELECT DISTINCT p.idPredicat, p.parPredicat, p.startPredicat, p.endPredicat, p.idCitation
			FROM predicat p, citation c
			WHERE p.idCitation = c.idCitation
			AND c.idDepeche_est_tiree_de = $id
			AND c.endCitation <> 0
		";
		$tpredicate = makePDOQuery($d,"SELECT", $q, array())->fetchAll();

                    // date table
                
                $q = "
			SELECT DISTINCT d.idDate, d.parDate, d.startDate, d.endDate, d.idCitation
			FROM date d, citation c
			WHERE d.idCitation = c.idCitation
			AND c.idDepeche_est_tiree_de = $id
			AND c.endCitation <> 0
		";
		$tdate = makePDOQuery($d,"SELECT", $q, array())->fetchAll();

		
		//print_r($taudience);
		
		$d = null;
		// Les num de paragraphes dans $t ont 1 de moins que dans $tocc.
		$mapping = array();
		/*
			$mapping[$paragraphe] = array($rawoffset01 => array($tag), ....);
		*/
                function check_open ($x) 
                {
                    return preg_match('/<[a-zA-Z]/',$x);
                }
                function check_close ($x) 
                {
                    return preg_match('%</%',$x);
                }
                
		function insertTag(&$map,$parag,$rawOffset,$tag) {
			if(!isset($map[$parag])) {
                            $map[$parag] = array();
			}
			if(!isset($map[$parag][$rawOffset])) {
                            $map[$parag][$rawOffset] = "";
			}
                        $v = $map[$parag][$rawOffset];
                        $otag = check_open($tag);
                        $ov = check_open($v);
                        if ($otag && !$ov) {
                                // opening tag always goes at the end, except
                                // for h3 !
                            if (preg_match('/<h3/',$tag)) {
                                $map[$parag][$rawOffset] = $tag.$v;
                            } else {
                                $map[$parag][$rawOffset] .= $tag;
                            }
                        } else if ($otag && $ov) {
                            if (preg_match('/class="occ"/',$tag)){
                                $map[$parag][$rawOffset] .= $tag;
                            } else if (preg_match('/class="verbatim"/',$tag)){
                                $map[$parag][$rawOffset] .= $tag;
                            } else {
                                $map[$parag][$rawOffset] = $tag . $v;
                            }
                        } else {
                            if (preg_match('/<h3/',$v)) {
                                $map[$parag][$rawOffset] .= $tag;
                            } else {
                                $map[$parag][$rawOffset] = $tag . $v;
                            }
                        }
		}
		// Début du titre
		insertTag($mapping,1,0,'<h3 id="titreDepeche">');
		
  		foreach($t as $c) {
			if(isset($c['idEntite']) && !empty($c['idEntite'])) {
				$marqueur = '<span id="quote-' . $c['idEntite'] . '-' . $c['idCitation'] . '" class="citation" title="par ' . my_decode($c['lblEntite']) . '">';
			}
			else { 
                            $marqueur = '<span id="quote-0-' . $c['idCitation'] . '" class="citation" title="par '.my_decode($c['txtAgent']).'">';
			} 
			$numPar = $c['parCitation'];
			
			insertTag($mapping,$numPar,$c['startCitation'],$marqueur);
			insertTag($mapping,$numPar,$c['endCitation'],"</span>");
		}
		
    	foreach($verbatims as $v) {
			$marqueur = '<span class="verbatim" id="verbatim-' . $v['idVerbatim'] . '">';
			insertTag($mapping,$v['parVerbatim'],$v['startVerbatim'],$marqueur);
			
			insertTag($mapping,$v['parVerbatim'],$v['endVerbatim'],"</span>");
        }
		
        foreach($tocc as  $numPar => $tentite) {
            foreach($tentite as $start => $tprop) {
                $end = $tprop['endOcc'];
                $idEn = $tprop['idEn'];
                $lblEn = $tprop['lblEntite'];
                if($tprop['txtEntite'] != null) $marqueur = '<span class="occ" title="' . my_decode($lblEn) . ' : ' . my_decode($tprop['txtEntite']) . '" id="disp-' . $numPar . '-' . $start . '-' . $end . '">';
                else $marqueur = '<span class="occ" title="' . my_decode($lblEn) . '" id="disp-' . $numPar . '-' . $start . '-' . $end . '">';
		
                insertTag($mapping,$numPar,$start,$marqueur);
                insertTag($mapping,$numPar,$end,"</span>");
            }
        }
    	foreach($tagent as $a) {
            if($a['lblEntite']) {
                $marqueur = '<span class="agent" id="agent-' . $a['idEntite'] . '-' . $a['idCitation'] . '" title="auteur : ' . my_decode($a['lblEntite']) . '">';
            }
            else {
                $marqueur = '<span class="agent" id="agent-' . $a['idEntite'] . '-' . $a['idCitation'] . '" title="auteur">';
            }
            
            insertTag($mapping,$a['parAgent'],$a['startAgent'],$marqueur);
            insertTag($mapping,$a['parAgent'],$a['endAgent'],"</span>");
            
        }
    	foreach($taudience as $a) {
            if(isset($a['idEntite'])) {
                $marqueur = '<span class="audience" id="audience-' . $a['idEntite'] . '-' . $a['idCitation'] . '" title="Audience : ' . my_decode($a['lblEntite']) . '">';
            }
            else {
                $marqueur = '<span class="audience" id="audience-0-' . $a['idCitation'] . '" title="audience">';
            }
            
            insertTag($mapping,$a['parAudience'],$a['startAudience'],$marqueur);
            insertTag($mapping,$a['parAudience'],$a['endAudience'],"</span>");
        }
        
        foreach($tpredicate as $p) {
            $marqueur = '<span class="predicat" id="predicat-0-' . $p['idCitation'] . '">';
            insertTag($mapping,$p['parPredicat'],$p['startPredicat'],$marqueur);
            insertTag($mapping,$p['parPredicat'],$p['endPredicat'],"</span>");
        }

        foreach($tdate as $p) {
            $marqueur = '<span class="date" id="date-0-' . $p['idCitation'] . '" title="date">';
            insertTag($mapping,$p['parDate'],$p['startDate'],$marqueur);
            insertTag($mapping,$p['parDate'],$p['endDate'],"</span>");
        }

		//mise en plus gros du titre
		insertTag($mapping,1,strlen($tpar[1]),'</h3>');
		
		// Affichages
		/* 
		echo "<!--";
		print_r($mapping);
		echo "-->";
		*/
		foreach($mapping as &$m) {
			ksort($m);
			$m = array_reverse($m, true);
		}
		$mapping = array_reverse($mapping,true);

 		foreach($tpar as $k => &$parag) {
			if(isset($mapping[$k])) {
				foreach($mapping[$k] as $offset => $tag) {
					//echo $offset . " - ";
					$parag = mb_substr($parag,0,$offset) . $tag . mb_substr($parag,$offset);
				}
			}
		}
		if(isset($_GET['identite'])) {
			echo '<span id="retour">Retourner aux citations de <a href="entite-' . $_GET['identite'] . '-' . urlencode($_GET['lblentite']) . '.html">' . my_decode($_GET['lblentite']) . '</a></span>';
		}
		/* 
		echo "<pre>";
		print_r($tpar);
		echo "</pre>";
		*/
		foreach($tpar as $p) {
			echo '<p>' . $p . "</p>\n\n";
		}
		echo '<br /><a href="' . $path . '">Lien vers le XML Agregate</a> - ';
		$tpath = explode('-',$path);
		echo $tpath[count($tpath) - 3] . "-" . $tpath[count($tpath) - 2] . "-" .  mb_substr($tpath[count($tpath) - 1],0,-4) . " © AFP";
	}
	else {
		echo "Erreur ! L'url que vous souhaitez consulter est mal formée ou l'entité n'existe plus.";
	}
?>

<script type="text/javascript" src="./javascript/size.js"></script>
<script type="text/javascript">

	var color = "maroon";
	function highlightOcc(o,on,classe) {
		//alert(o.innerHTML);
		var f = on | false;
		var c = classe;
		var id = (o.attributes.getNamedItem('id').nodeValue);
		var tid =  id.split("-");
		var occ = document.getElementById('disp-' + tid[1] + '-' + tid[2] + '-' + tid[3]);
		if(!occ) return;
		if(occ.attributes.getNamedItem('class').nodeValue.split(" ")[2] == "selectOcc" && !f) {
			occ.setAttribute('class','occ');
		}
		else {
			occ.setAttribute('class','occ ' + classe + ' selectOcc');
		}
		
	}
	
	// e est la div "title" du panel
	function highlightEn(e,forcer) {
		var f = forcer | false;
		var tocc = e.parentNode.childNodes[1].childNodes;
		var classe = e.attributes.getNamedItem('title').nodeValue.split(" ")[0];
		//alert(classe);
		if(e.attributes.getNamedItem('class').nodeValue.split(" ")[2] == "select" && !f) {
			e.setAttribute('class','occ');
		}
		else {
			e.setAttribute('class','occ ' + classe + ' select');
		}
		
		var tquotes = getElementsByClassName("citation","span",document.getElementById('middle'));
		tquotes = tquotes.concat(getElementsByClassName("citationSelec","span",document.getElementById('middle')));
		
		var idEn = e.attributes.getNamedItem('id').nodeValue.split("-")[1];
		
		var taudience = getElementsByClassName("audience","span",document.getElementById('middle'));
		taudience = taudience.concat(getElementsByClassName("audienceSelec","span",document.getElementById('middle')));
		
		for(var i=0;i<tquotes.length;++i) {
			var idAut = tquotes[i].attributes.getNamedItem('id').nodeValue.split("-");
			if(idAut[1] == idEn) {
				if(tquotes[i].attributes.getNamedItem('class').nodeValue == "citation" || f) {
					tquotes[i].setAttribute('class','citationSelec');
				}
				else {
					tquotes[i].setAttribute('class','citation');
				}
				for(var j=0;j<taudience.length;++j) {
					var idAud = taudience[j].attributes.getNamedItem('id').nodeValue.split("-");
					if(idAut[2] == idAud[2]) {
						if(taudience[j].attributes.getNamedItem('class').nodeValue == "audience" || f) {
							taudience[j].setAttribute('class','audienceSelec');
						}
						else {
							taudience[j].setAttribute('class','audience');
						}
					}
				}
			}
		}

		
		var tagents = getElementsByClassName("agent","span",document.getElementById('middle'));
		tagents = tagents.concat(getElementsByClassName("agentSelec","span",document.getElementById('middle')));
		for(var i=0;i<tagents.length;++i) {
			var idAut = tagents[i].attributes.getNamedItem('id').nodeValue.split("-");
			if(idAut[1] == idEn) {
				if(tagents[i].attributes.getNamedItem('class').nodeValue == "agent" || f) {
					tagents[i].setAttribute('class','agentSelec');
				}
				else {
					tagents[i].setAttribute('class','agent');
				}
			}
		}
		for(var i=0;i<tocc.length;++i) {
			highlightOcc(tocc[i],f,classe);
		}
	}
	
	function resetHighlight() {
		var ens = getElementsByClassName("occ(.*)select","div",document.getElementById('listeEn'));
		ens  = ens.concat(getElementsByClassName("occ(.*)selectOcc","span",document.getElementById('middle')));
		for(var i=0;i<ens.length;++i) {
			ens[i].setAttribute('class','occ');
		}
		
		var quotes = getElementsByClassName("citationSelec","span",document.getElementById('middle'));
		for(var i=0;i<quotes.length;++i) {
			quotes[i].setAttribute('class','citation');
		}
		var agents = getElementsByClassName("agentSelec","span",document.getElementById('middle'));
		for(var i=0;i<agents.length;++i) {
			agents[i].setAttribute('class','agent');
		}
		var audiences = getElementsByClassName("audienceSelec","span",document.getElementById('middle'));
		for(var i=0;i<audiences.length;++i) {
			audiences[i].setAttribute('class','audience');
		}
	}
	function haveCitation(en) {
		var quotes = getElementsByClassName("citation(.*)","span",document.getElementById('middle'));
		var idEn = en.attributes.getNamedItem('id').nodeValue.split("-")[1];
		for(var i=0;i<quotes.length;++i) {
			if(idEn == quotes[i].attributes.getNamedItem('id').nodeValue.split("-")[1]) return true;
		}
		return false;
	}
	function highlightAll(e) {
		var ens = getElementsByClassName("occ(.*)","div",document.getElementById('listeEn'));
		var on = "Sélectionner toutes les entités";
		var off = "Déselectionner les entités";		
		
		
		for(var i=0;i<ens.length;++i) {
			highlightEn(ens[i], true);
		}
		if(e.innerHTML == on) {
			e.innerHTML = off;
		}
		else {
			e.innerHTML = on;
			for(var i=0;i<ens.length;++i) {
				highlightEn(ens[i], false);
			}	
		}
	}

function show_all (kinds) {
    for (var k=0;k < kinds.length;++k) {
        var kind = kinds[k];
        var t = getElementsByClassName(kind + "(.*)","span",document.getElementById('middle'));
        for(var i=0;i<t.length;++i) {
            for(var i=0;i<t.length;++i) {
                var c = t[i].attributes.getNamedItem('class').nodeValue;
                c = c.substring(0,c.length-9);
                t[i].setAttribute('class',c);
            }
        }
    }
}

function hide_all (kinds) {
    for (var k=0;k < kinds.length;++k) {
        var kind = kinds[k];
        var t = getElementsByClassName(kind + "(.*)","span",document.getElementById('middle'));
        for(var i=0;i<t.length;++i) {
            for(var i=0;i<t.length;++i) {
                var c = t[i].attributes.getNamedItem('class').nodeValue;
                t[i].setAttribute('class',c + ' invisible');
            }
        }
    }
}

	function highlightAllCitation(e) {
		var on = "Afficher toutes les citations";
		var off = "Masquer les citations";
                var kinds = ["citation","agent","audience","predicat","date"];
                
		if(e.innerHTML == on) {
			e.innerHTML = off;
                        show_all(kinds);
                }
		else {
                    e.innerHTML = on;
                    hide_all(kinds);
		}
	}

	var body = document.getElementById('body');
	var panel = document.createElement('div');
	var left = document.getElementById('left');
	
	left.style.width = "75%";
	left.setAttribute('class','seq');
	
	panel.setAttribute('id','panel');
	panel.setAttribute('class','colonne');
	body.nextSibbling;
	body.appendChild(panel);
	
	var listeEn = document.createElement('div');
	listeEn.setAttribute('id','listeEn');
	
	// Mise en place de HighlightAll et unHighlightAll
	var divHighlightAll = document.createElement('div');
	divHighlightAll.setAttribute('id','divHighlightAll');
	divHighlightAll.setAttribute('class','seq occ');
	divHighlightAll.innerHTML = "Sélectionner toutes les entités";
	divHighlightAll.onclick = function() {
		highlightAll(this);
	}
	listeEn.appendChild(divHighlightAll);
	
	var divHighlightAllCitation = document.createElement('div');
	divHighlightAllCitation.setAttribute('id','divHighlightAllCitation');
	divHighlightAllCitation.setAttribute('class','seq occ');
	divHighlightAllCitation.innerHTML = "Masquer les citations";	
	divHighlightAllCitation.onclick = function() {
		highlightAllCitation(this);
	}	
	listeEn.appendChild(divHighlightAllCitation);
	
	
	panel.appendChild(listeEn);
	<?php
	
	function cmp_tiden($a,$b) {
		if($a[0]['lblEntite'] == $b[0]['lblEntite']) if($a['startCitation'] == $b['startCitation']) return 0;
		return ($a[0]['lblEntite'] < $b[0]['lblEntite']) ? -1 : 1;

	}

function build_en_label ($occ) 
{
    $href = $occ['linkEntite'];
    $htype = $occ['typeLinkEntite'];
    $url = "";
    if ($htype && $href) {
        $href = addslashes($href);
        if ($htype == "Wikipedia") {
            $url .= '<a target="_blank" href="'.$href.'"><img src="'.VAR_PATH_TEMPLATES . VAR_TEMPLATE .'images/Wikipedia-globe-16x16.png"/></a> ';
        } else {
            $url .= '<a target="_blank" href="'.$href.'"><img width="16px" src="'.VAR_PATH_TEMPLATES . VAR_TEMPLATE .'images/google-earth.png"/></a> ';
        }
    }
    $url .= my_decode(addslashes($occ['lblEntite']));
    echo $url;
}

	usort($tiden, "cmp_tiden");
	foreach($tiden as $en) {
		?>
		var main = document.createElement('div');
		main.setAttribute('id','EN-<?php echo $en[0]['idEntite']; ?>-panel');
		var title = document.createElement('div');
		title.innerHTML += '<span class="<?php echo $en[0]['lblTypeEntite']; ?>">►</span> <?php build_en_label($en[0]); ?>';
		title.setAttribute('id','EN-<?php echo $en[0]['idEntite']; ?>-title');
		title.setAttribute('class','occ');
		title.setAttribute('title','<?php echo $en[0]['lblTypeEntite'] . " : " . addslashes($en[0]['txtEntite']); ?>');
		
		listeEn.appendChild(main);
		main.appendChild(title);
		// Boucle de génération des nodes du panel
		var liste = document.createElement('div');
		liste.style.display = "none";
		main.appendChild(liste);
	<?php
		// Serait plus joli : faire des structures en echo de JavaScript et boucler sur le javascript
		sort($en);
		$str = "var tocc = new Array(";
		foreach($en as $occ) {
			$str .= "new Array('" . addslashes($occ['lblOccurrence']) . "', '" . $occ['parOccurrence'] . "-" . $occ['startOccurrence'] . "-" . $occ['endOccurrence'] . "', '" . $occ['idEntite'] . "'),";
		}
		$str = mb_substr($str,0,-1);
		$str .= ");";
		echo $str;
	?>
		for(var i=0;i<tocc.length;++i) {
			var occ = document.createElement('div');
			occ.innerHTML = tocc[i][0];
			occ.setAttribute('id','panel-' + tocc[i][1]);
			liste.appendChild(occ);
		}
   		title.onclick = function () {
			//alert(this.attributes.getNamedItem('class').nodeValue);
			if(this.attributes.getNamedItem('class').nodeValue.split(" ")[2] == "select") {
				highlightEn(this);
			}
			else {
				resetHighlight(); 
				highlightEn(this);
			}
		};
	<?php
	}
		
	?>
        
	// Fin de la boucle de génération des nodes du panel
	
	// Génération des clicks dans le texte
	
	var occText = getElementsByClassName("occ(.*)","span",document.getElementById('middle'));
	for(var i=0;i<occText.length;++i) {
		occText[i].onclick = function () {
			var tidPanel = this.attributes.getNamedItem('id').nodeValue.split('-');
			var elemPanel = document.getElementById("panel-" + tidPanel[1] + "-" + tidPanel[2] + "-" + tidPanel[3]);
			if(elemPanel.parentNode.parentNode.firstChild.attributes.getNamedItem('class').nodeValue.split(" ")[2] == "select") {
				highlightEn(elemPanel.parentNode.parentNode.firstChild);
			}
			else {
				resetHighlight(); 
				highlightEn(elemPanel.parentNode.parentNode.firstChild);
			}
		};
	}
	<?php
	if(isset($_GET['identite'])) {?>
			var toHigh = getElementsByClassName("occ(.*)","div",document.getElementById('listeEn'));
			var quotes = getElementsByClassName("citation(.*)","span",document.getElementById('middle'));
			
			//alert(quotes.length);
			var cpt = 0;
			for(var i=0;i<toHigh.length;++i) {
				var id = toHigh[i].attributes.getNamedItem('id').nodeValue.split('-')[1];
				if(id == (<?php echo $_GET['identite'] ?>)) {
					highlightEn(toHigh[i]);
				}
			}
		<?php
		}
	?>
	feetHSmallCol('left', 'panel');
	feetHSmallCol('panel', 'left');
	/*
		Le panel suit le scroll
	*/
	var top = listeEn.offsetHeight;
	var stop = listeEn.style.top;
	listeEn.style.width = listeEn.offsetWidth + "px";
	document.onscroll = function () {
		if(top < document.documentElement.scrollTop) {
			listeEn.style.zIndex = "100";
			listeEn.style.top = "0px";
			listeEn.style.position = "fixed";
		}
		else {
			listeEn.style.height = top + "px";
			listeEn.style.top = stop;
			listeEn.style.position = "absolute";
		}
	};
	
	// Génération des * pour les entités ayant {1,} citations associées
	var occ = getElementsByClassName("occ(.*)","div",document.getElementById('listeEn'));
	var quotes = getElementsByClassName("citation(.*)","span",document.getElementById('middle'));

	//alert(quotes.length);
	for(var i=0;i<occ.length;++i) {
		var cpt = 0;
		var id = occ[i].attributes.getNamedItem('id').nodeValue.split('-')[1];
		for(var j=0;j<quotes.length;++j){
			//alert(quotes[j].attributes.getNamedItem('id').nodeValue);
			if(quotes[j].attributes.getNamedItem('id').nodeValue.split('-')[1] == id) {
				cpt++;
			}
		}
		if(cpt) {
			occ[i].innerHTML += ' (' + cpt + ')';
		}
//                occ[i].innerHTML += ' <a href="foo">foo</foo> ';

	}

	/* 
	listeEn.style.position = "fixed";
	 */
	
	
</script>