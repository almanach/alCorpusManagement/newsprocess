<?php 
//header('Content-Type: text/html; charset=UTF-8');
$lbl = my_decode($_GET['lblentite']);
$id = $_GET['identite'];

// VÃ©rification qu'il n'y a pas de fourberies dans la requÃªte :
if(isset($_GET['lblslug'])) {
	$q = "SELECT c.*, d.titreDepeche, d.txtDepeche, d.idDepeche, d.dateDepeche, e.txtEntite
		FROM citation c, entite e, depeche d, assoccitationentite ace, iptc i, assocdepecheiptc adi
		WHERE e.idEntite = :id
		AND e.lblEntite = :lbl
		AND e.idEntite = ace.idEntite
		AND ace.idCitation = c.idCitation
		AND d.idDepeche = c.idDepeche_est_tiree_de
                AND i.lblIPTC = '" . ($_GET['lblslug']) ."'
                AND i.idIPTC = adi.idIPTC
                AND adi.idDepeche = d.idDepeche;
	";
}
else {
	$q = "SELECT c.*, d.titreDepeche, d.txtDepeche, d.idDepeche, d.dateDepeche, e.txtEntite
		FROM citation c, entite e, depeche d, assoccitationentite ace
		WHERE e.idEntite = :id
		AND e.lblEntite = :lbl
		AND e.idEntite = ace.idEntite
		AND ace.idCitation = c.idCitation
		AND d.idDepeche = c.idDepeche_est_tiree_de;
	";
}

	include_once('./common/sql/query.php');
	$d = connex();
	$tin = array(
		':lbl' => array($lbl, PDO::PARAM_STR),
		':id' => array($id, PDO::PARAM_INT),
	);
	$result = makePDOQuery($d,"SELECT", $q, $tin);
	$res = $result->fetchAll();
	$d = null;
	if(count($res)) {
		function truncate($string, $max, $replacement) {
			if (mb_strlen($string) <= $max) {
				return $string;
			}
			$leave = $max - mb_strlen ($replacement);
			return mb_substr($string, 0, $max - mb_strlen($replacement)) . $replacement;
		}
		
		$tmp = array();
		for($i=0;$i<count($res);++$i) {
			$tmp[$i] = $res[$i]['idDepeche'];
		}
		$tmp = array_unique($tmp);
		
		echo "Citations de <b>" . my_decode($lbl) . "</b>";
		if($res[0]['txtEntite'] != null && $res[0]['txtEntite'] != '(UNKNOWN PERSON)') echo " (" . my_decode($res[0]['txtEntite']) . ")";
		echo " :";
		echo "<table border=\"0\" class=\"depeche\">";
		
		echo "<tr>";
		echo '<th width="60%">Dépêches : '. count($tmp) . '</th>';
		echo '<th width="40%">Citations de ' . my_decode($lbl) . ' : ' . count($res) . '</th>';
		echo "</tr>";
		$nbRow = 0;
		for($i=0;$i<count($res);++$i) {
			echo "<tr";
			echo ">\n";
			$j = 1;
			$k = $i;
			if(!isset($res[$i-1])) {
				while(isset($res[$k+1]) && $res[$k]['titreDepeche'] == $res[$k+1]['titreDepeche']) {
					$j++;
					$k++;
				}
				echo '<td';
				if($nbRow%2 == 0) echo ' style="background-color:#d9d9e9;"';
				echo ' rowspan="' . $j . '"><a href="./entite-' . $id . '-' . urlencode($lbl) . '-depeche-' . $res[$i]['idDepeche'] . '-citation-' . $res[$i]['idCitation'] . '.html">AFP, ' . $res[$i]['dateDepeche'] . '<br /><b>' . my_decode($res[$i]['titreDepeche']) . '</b></a><br />' . my_decode(truncate($res[$i]['txtDepeche'],200," ...")) . '</td>' . "\n";
				$nbRow++;
			}
			elseif($res[$i]['titreDepeche'] != $res[$i-1]['titreDepeche']) {
				while(isset($res[$k+1]) && $res[$k]['titreDepeche'] == $res[$k+1]['titreDepeche']) {
					$j++;
					$k++;
				}
				echo '<td';
				if($nbRow%2 == 0) echo ' style="background-color:#d9d9e9;"';
				echo ' rowspan="' . $j . '"><a href="./entite-' . $id . '-' . urlencode($lbl) . '-depeche-' . $res[$i]['idDepeche'] . '-citation-' . $res[$i]['idCitation'] . '.html">AFP, ' . $res[$i]['dateDepeche'] . '<br /><b>' . my_decode($res[$i]['titreDepeche']) . '</a></b><br />' . my_decode(truncate($res[$i]['txtDepeche'],200," ...")) . '</td>' . "\n";
				$nbRow++;
			}
			//echo '<td>AFP, ' . $res[$i]['dateDepeche'] . '<br /><b>' . truncate(($res[$i]['titreDepeche']),200,"...") . '</b></td>';		
			echo '<td';
			if($nbRow%2 == 1) echo ' style="background-color:#d9d9e9;"';
			echo '> - ' . my_decode(truncate($res[$i]['txtCitation'],200, "...")) . '</td>' . "\n";
			echo"</tr>\n";
		}
		echo "</table>";
	}
	else {
		echo "Entite : Erreur ! L'url que vous souhaitez consulter est mal formÃ©e ou l'entitÃ© n'existe plus.";
	}
?>