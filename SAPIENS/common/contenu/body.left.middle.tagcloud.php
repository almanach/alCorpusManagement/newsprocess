<?php
include_once("common/vars.php");
// Nombre d'élément du nuage


	// prend en entrée : le max et le nombre d'occurences.
	function getSize($m,$o) {
		$taille_maximum = VAR_CLOUD_MAX_SIZE;
		$taille_minimum = VAR_CLOUD_MIN_SIZE;
		
		$t = $taille_maximum - $taille_minimum;
		
		$res = ($o/$m) * $t;
		return (int) $res + $taille_minimum;
		//return $res + $taille_minimum;
	}

	function getColor($m,$o) {
		$g = 0;
		$r = ($o/$m) * 255;
		$b = (($m-$o)/$m) * 255;
		
		return "rgb(" . (int) $r . "," . $g . "," . (int) $b . ")";
	}
	
	// + Rouge = + occurences 
	// + Bleu = - occurences
	/*
	Selectionner des requetes au hasard : 
	// Grosseur selon le nombre absolu de citation
	$q = "
 		SELECT e.lblEntite, e.idEntite,
			(SELECT COUNT(c.idEntite_possede) 
			FROM citation c
			WHERE e.idEntite = c.idEntite_possede) AS VAR_COUNT_NB
		FROM entite e, citation c
		WHERE e.idEntite = c.idEntite_possede
		GROUP BY c.idEntite_possede
		ORDER BY RAND()
		LIMIT $limit; 
	";
	// Grosseur selon : 1 points pour une ou plusieurs apparition(s) de l'EN dans une dépêche.
	$q = "
	SELECT DISTINCT e.lblEntite, e.idEntite, (
		SELECT (COUNT(DISTINCT o.idDepeche)) as subNB
		FROM occurrence o
		WHERE o.idEntite = e.idEntite
		GROUP BY o.idEntite
		) as VAR_COUNT_NB
	FROM occurrence o, entite e, citation c
	WHERE e.idEntite = c.idEntite_possede
	ORDER BY RAND()
	LIMIT $limit
	";
	*/
        $prefix="";
	if(isset($_GET['lblslug'])) {
            $prefix="slug-".$_GET['lblslug']."-";
		$q = "
			SELECT e.lblEntite, e.idEntite, e.txtEntite,
				(SELECT COUNT(DISTINCT c.idDepeche_est_tiree_de) 
				FROM citation c, assoccitationentite ace
				WHERE e.idEntite = ace.idEntite
				AND ace.idCitation = c.idCitation) AS VAR_COUNT_NB
			FROM entite e, citation c, assoccitationentite ace, typeentite te, iptc i, assocdepecheiptc adi, depeche d
			WHERE e.idEntite = ace.idEntite
			AND e.idTypeEntite_appartient_a = te.idTypeEntite
			AND ace.idCitation = c.idCitation
			AND i.lblIPTC = '" . ($_GET['lblslug']) ."'
			AND i.idIPTC = adi.idIPTC
			AND adi.idDepeche = d.idDepeche
			AND c.idDepeche_est_tiree_de = d.idDepeche
			AND te.lblTypeEntite <> 'np'
			GROUP BY ace.idEntite
			ORDER BY e.lblEntite;
		";
	}
	else {
		$q = "
			SELECT e.lblEntite, e.idEntite, e.txtEntite,
				(SELECT COUNT(DISTINCT c.idDepeche_est_tiree_de) 
				FROM citation c, assoccitationentite ace
				WHERE e.idEntite = ace.idEntite
				AND ace.idCitation = c.idCitation) AS VAR_COUNT_NB
			FROM entite e, citation c, assoccitationentite ace, typeentite te
			WHERE e.idEntite = ace.idEntite
			AND e.idTypeEntite_appartient_a = te.idTypeEntite
			AND ace.idCitation = c.idCitation
			AND te.lblTypeEntite <> 'np'
			GROUP BY ace.idEntite
			ORDER BY e.lblEntite;
		";
	}
	include_once('./common/sql/query.php');
	$d = connex();
	
	//echo $q;
	$res = makePDOQuery($d,"SELECT", $q, array());
	$res = $res->fetchAll();
	
	$max = 0;
	if(!isset($min)) $min = 0;
	for($i=0;$i<count($res);++$i) {
		if($res[$i]['VAR_COUNT_NB'] > $max) $max = $res[$i]['VAR_COUNT_NB'];
		if(((int) $res[$i]['VAR_COUNT_NB']) < $min) {
			$res[$i] = null;
			//var_dump($res[$i]);
		}
	}
	$d = null;
	// Appliquer le style selon le max.
	if(count($res)) {
		foreach($res as $cloud) {
			if($cloud == null) continue;
			//$url = strtr($cloud['lblEntite'], 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
			$url = urlencode($cloud['lblEntite']);
			$lblEntite = str_replace(" ", "&nbsp;", $cloud['lblEntite']);
			?>
			<a class="cloud" style="font-size:<?php echo getSize($max,$cloud['VAR_COUNT_NB']);?>px;color:<?php echo getColor($max,$cloud['VAR_COUNT_NB']);?>;" href="./<?php echo $prefix;?>entite-<?php echo $cloud['idEntite'];?>-<?php echo $url;?>.html" title="<?php echo $cloud['txtEntite']; ?>"><?php 
				if(isset($_GET['lblslug'])) echo my_decode($lblEntite);
				else echo ($lblEntite);
			?></a>
			<?php
		}
	}
	else {
		echo "<br /><br />Aucun résultat à afficher.";
	}

?>	