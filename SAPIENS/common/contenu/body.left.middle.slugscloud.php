<?php
include_once("common/vars.php");
// Nombre d'élément du nuage
//$limit = 100;
$limit = 200;
	// prend en entrée : le max et le nombre d'occurences.
	function getSize($m,$o) {
		$taille_maximum = VAR_CLOUD_MAX_SIZE;
		$taille_minimum = VAR_CLOUD_MIN_SIZE;
		
		$t = $taille_maximum - $taille_minimum;
		
		$res = ($o/$m) * $t;
		return (int) $res + $taille_minimum;
		//return $res + $taille_minimum;
	}

	function getColor($m,$o) {
		$g = 0;
		$r = ($o/$m) * 255;
		$b = (($m-$o)/$m) * 255;
		
		return "rgb(" . (int) $r . "," . $g . "," . (int) $b . ")";
	}
	
	// + Rouge = + occurences 
	// + Bleu = - occurences
	
/* 	$q = "
		SELECT i.lblIPTC, COUNT(i.lblIPTC) as VAR_COUNT_NB
		FROM iptc i
		WHERE i.lblIPTC <> 'Présidentielle'
		AND	 i.lblIPTC <> '2007'
		GROUP BY i.lblIPTC
		ORDER BY RAND()
		LIMIT $limit;
	"; */
	$q = "SELECT i.lblIPTC, COUNT(i.lblIPTC) as VAR_COUNT_NB
		FROM iptc i
		WHERE i.lblIPTC <> 'Présidentielle'
		AND	 i.lblIPTC <> '2007'
		AND i.idIPTC IN (
			SELECT adi.idIPTC
			FROM assocdepecheiptc adi, depeche d, citation c, assoccitationentite ace
			WHERE adi.idDepeche = d.idDepeche
			AND d.idDepeche = c.idDepeche_est_tiree_de
			AND c.idCitation = ace.idCitation
		)
		GROUP BY i.lblIPTC
		ORDER BY RAND()
		LIMIT $limit
	";
	include_once('./common/sql/query.php');
	$d = connex();
	
	$res = makePDOQuery($d,"SELECT", $q, array());
	$res = $res->fetchAll();
	echo "<pre>";
	//print_r($res);
	echo "</pre>";
	$d = null;
	
	$max = 0;
	for($i=0;$i<count($res);++$i) {
		if($res[$i]['VAR_COUNT_NB'] > $max) $max = $res[$i]['VAR_COUNT_NB'];
	}
	?>
	<?php
	foreach($res as $cloud) {
	//$url = strtr($cloud['lblIPTC'], 'ÀÁÂÃÄÅÇÈÉÊËÌÍÎÏÒÓÔÕÖÙÚÛÜÝàáâãäåçèéêëìíîïðòóôõöùúûüýÿ', 'AAAAAACEEEEIIIIOOOOOUUUUYaaaaaaceeeeiiiioooooouuuuyy');
	$url = urlencode($cloud['lblIPTC']);
	?>
		<a class="cloud" style="font-size:<?php echo getSize($max,$cloud['VAR_COUNT_NB']);?>px;color:<?php echo getColor($max,$cloud['VAR_COUNT_NB']);?>;" href="./slug-<?php echo my_decode($url);?>.html"><?php 
			if(!isset($_SERVER['REDIRECT_URL'])) echo ($cloud['lblIPTC']);
			else echo my_decode($cloud['lblIPTC']);
		?></a>
		<?php
	}
?>
		