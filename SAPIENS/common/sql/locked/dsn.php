<?php
include_once('config.php');
function connex() {
	$dsn = SQL_ENGINE . ":dbname=" . SQL_BASE . ";host=" . SQL_HOST;

	try {
		return new PDO($dsn, SQL_USER, SQL_PASSWORD);	
		
	} catch (PDOException $e) {
		die('Connexion impossible'); //-> plus s�r.
		//die('Connexion �chou�e : ' . $e->getMessage()); -> Uniquement pour du debug.
	}
}
?>
