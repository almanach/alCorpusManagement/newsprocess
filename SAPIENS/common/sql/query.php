<?php
include("locked/dsn.php");
function makePDOQuery($d,$type,$query,$tbind) {
	switch($type) {
		case "SELECT" :
		case "INSERT" :
		case "UPDATE" :
			$q = $d->prepare($query);
			
			PDOBindArray($q,$tbind);
			
			$q->execute();

			if($q->errorCode() != "00000") {
				return $q->errorCode(); // Ou pas ?
			}
			else {
				return $q;
			}
			break;
		
	}
}
function PDOBindArray(&$poStatement, &$paArray) {
	foreach ($paArray as $k=>$v) {
		@$poStatement->bindValue($k, $v[0], $v[1]);
	}
}
?>