<?php
				
				$query = "
					SELECT e.lblEntite, e.idEntite,
						(SELECT COUNT(c.idEntite_possede) 
						FROM citation c
						WHERE e.idEntite = c.idEntite_possede) AS VAR_COUNT_NB
					FROM entite e, citation c
					WHERE e.idEntite = c.idEntite_possede
					GROUP BY c.idEntite_possede
					ORDER BY VAR_COUNT_NB DESC;
				";
				$q = $d->prepare($query);
				$q->execute();
	
				if($q->errorCode() != "00000") {
					die("Erreur : " . $q->errorCode()); // Ou pas ?
				}
				else {
					$resultat = $q->fetchAll();
				}
?>