<?php
// Titre du site :
define('VAR_TITLE','SAPIENS'); //
define('VAR_TITLE_ESCAPED',str_replace(' ','_',VAR_TITLE));
// Pr�sentation du site :
define('VAR_DEF','Station d\'Analyse Profonde via Internet pour l\'Exploration de NewS');
// SAPIENS ? :: Station d'Analyse du Parsing Internet d'Entit�s Nomm�es SAPIENS

// Mail principale :
define('VAR_MAIN_MAIL','vmignot@gmail.com'); //

/*
.: CONSTANTES ARCHITECTURALES :.
	index.php fait appel aux pages situ�es dans /common/pages/
	Ces pages font ensuite appel aux pages dans VAR_PATH_TEMPLATES . VAR_TEMPLATE
		Pour changer l'architecture du site, il faut modifier ce chemin, bien que modifier le CSS directement est �galement possible.
	Les pages de template font ensuite appel aux pages de VAR_CONTENU, mis dans une constante pour faciliter son �dition en cas de besoin : il reste plus simple de l'impl�menter tel quel, et de ne changer que le templates ou le css. Pr�voyez un Backup du CSS original.
*/
define('VAR_CLOUD_MAX_SIZE','34');
define('VAR_CLOUD_MIN_SIZE','15');

// 
define('VAR_ROOT', $_SERVER['DOCUMENT_ROOT'] . "SAPIENS/");
// Constante du chemin des templates
define('VAR_PATH_TEMPLATES','templates/'); // def : templates/

// Constante permettant d'utiliser un autre template du dossier VAR_PATH_TEMPLATES
define('VAR_TEMPLATE','default/'); // def : default/

// Constante permettant d'utiliser un autre dossier de contenu 
define('VAR_CONTENU','common/contenu/'); //def : common/contenu/


?>
