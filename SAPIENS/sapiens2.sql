-- phpMyAdmin SQL Dump
-- version 3.0.1.1
-- http://www.phpmyadmin.net
--
-- Serveur: localhost
-- Généré le : Ven 19 Juin 2009 à 17:08
-- Version du serveur: 5.1.30
-- Version de PHP: 5.2.7

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `sapiens2`
--

-- --------------------------------------------------------

--
-- Structure de la table `agent`
--

CREATE TABLE `agent` (
  `idAgent` int(11) NOT NULL AUTO_INCREMENT,
  `idCitation` int(11) NOT NULL,
  `idEntite` int(11) NOT NULL,
  `parAgent` int(11) NOT NULL,
  `startAgent` int(11) NOT NULL,
  `endAgent` int(11) NOT NULL,
  `txtAgent` text NOT NULL,
  PRIMARY KEY (`idAgent`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2075 ;

-- --------------------------------------------------------

--
-- Structure de la table `assoccitationentite`
--

CREATE TABLE `assoccitationentite` (
  `idCitation` int(11) NOT NULL,
  `idEntite` int(11) NOT NULL,
  PRIMARY KEY (`idCitation`,`idEntite`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `assocdepecheiptc`
--

CREATE TABLE `assocdepecheiptc` (
  `idIPTC` int(11) NOT NULL DEFAULT '0',
  `idDepeche` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idIPTC`,`idDepeche`),
  KEY `fk_AssocDepecheIPTCToDepecheBypossede_idDepeche` (`idDepeche`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `audience`
--

CREATE TABLE `audience` (
  `idAudience` int(11) NOT NULL AUTO_INCREMENT,
  `parAudience` int(11) NOT NULL,
  `startAudience` int(11) NOT NULL,
  `endAudience` int(11) NOT NULL,
  `entityAudience` int(11) NOT NULL,
  `isIndirectAudience` tinyint(1) NOT NULL DEFAULT '0',
  `valueAudience` text NOT NULL,
  `idCitation` int(11) NOT NULL,
  PRIMARY KEY (`idAudience`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Structure de la table `date`
--

CREATE TABLE `date` (
  `idDate` int(11) NOT NULL AUTO_INCREMENT,
  `parDate` int(11) NOT NULL,
  `startDate` int(11) NOT NULL,
  `endDate` int(11) NOT NULL,
  `entityDate` int(11) NOT NULL,
  `isIndirectDate` tinyint(1) NOT NULL DEFAULT '0',
  `valueDate` text NOT NULL,
  `idCitation` int(11) NOT NULL,
  PRIMARY KEY (`idDate`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=86 ;

-- --------------------------------------------------------

--
-- Structure de la table `citation`
--

CREATE TABLE `citation` (
  `idCitation` int(11) NOT NULL AUTO_INCREMENT,
  `txtCitation` text,
  `parCitation` int(11) DEFAULT NULL,
  `startCitation` int(11) DEFAULT NULL,
  `endCitation` int(11) DEFAULT NULL,
  `idDepeche_est_tiree_de` int(11) NOT NULL,
  PRIMARY KEY (`idCitation`),
  KEY `fk_CitationToDepecheByest_tiree_de_idDepeche` (`idDepeche_est_tiree_de`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2643 ;

-- --------------------------------------------------------

--
-- Structure de la table `depeche`
--

CREATE TABLE `depeche` (
  `idDepeche` int(11) NOT NULL AUTO_INCREMENT,
  `txtDepeche` text,
  `titreDepeche` text NOT NULL,
  `dateDepeche` date NOT NULL,
  `pathDepeche` varchar(255) NOT NULL,
  `creationTimeDepeche` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`idDepeche`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=836 ;

-- --------------------------------------------------------

--
-- Structure de la table `entite`
--

CREATE TABLE `entite` (
  `idEntite` int(11) NOT NULL AUTO_INCREMENT,
  `lblEntite` varchar(255) DEFAULT NULL,
  `idTypeEntite_appartient_a` int(11) NOT NULL,
  `txtEntite` text,
  `linkEntite` text,
  `typeLinkEntite` varchar(511) DEFAULT NULL,
  PRIMARY KEY (`idEntite`),
  KEY `fk_EntiteToTypeEntiteByappartient_a_idTypeEntite` (`idTypeEntite_appartient_a`),
  KEY `lblEntite` (`lblEntite`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2680 ;

-- --------------------------------------------------------

--
-- Structure de la table `iptc`
--

CREATE TABLE `iptc` (
  `idIPTC` int(11) NOT NULL AUTO_INCREMENT,
  `lblIPTC` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idIPTC`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2869 ;

-- --------------------------------------------------------

--
-- Structure de la table `occurrence`
--

CREATE TABLE `occurrence` (
  `idEntite` int(11) NOT NULL,
  `idDepeche` int(11) NOT NULL,
  `lblOccurrence` varchar(255) NOT NULL,
  `parOccurrence` int(11) NOT NULL,
  `startOccurrence` int(11) NOT NULL,
  `endOccurrence` int(11) NOT NULL,
  PRIMARY KEY (`idEntite`,`idDepeche`,`parOccurrence`,`startOccurrence`),
  KEY `fk_OccurrenceToDepeche_idDepeche` (`idDepeche`),
  KEY `fk_OccurrenceToEntite_idEntite` (`idEntite`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 ROW_FORMAT=DYNAMIC;

-- --------------------------------------------------------

--
-- Structure de la table `predicat`
--

CREATE TABLE `predicat` (
  `idPredicat` int(11) NOT NULL AUTO_INCREMENT,
  `parPredicat` int(11) NOT NULL,
  `startPredicat` int(11) NOT NULL,
  `endPredicat` int(11) NOT NULL,
  `idCitation` int(11) NOT NULL,
  PRIMARY KEY (`idPredicat`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `typeentite`
--

CREATE TABLE `typeentite` (
  `idTypeEntite` int(11) NOT NULL AUTO_INCREMENT,
  `lblTypeEntite` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idTypeEntite`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

-- --------------------------------------------------------

--
-- Structure de la table `verbatim`
--

CREATE TABLE `verbatim` (
  `idVerbatim` int(11) NOT NULL AUTO_INCREMENT,
  `parVerbatim` int(11) NOT NULL,
  `startVerbatim` int(11) NOT NULL,
  `endVerbatim` int(11) NOT NULL,
  `txtVerbatim` text NOT NULL,
  `idDepeche` int(11) NOT NULL,
  PRIMARY KEY (`idVerbatim`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6079 ;
