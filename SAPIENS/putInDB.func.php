<?php
function putInDB($f,$d, $forcer = false) {
	$base = SQL_BASE;
	// $str = "";
	$q = "
		SELECT d.idDepeche
		FROM `$base`.depeche d
		WHERE d.pathDepeche = :pd
		LIMIT 0,1
	";
	
	$tmp = array(
		':pd' => array($f, PDO::PARAM_STR)
	);
	$res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();	
	if(count($res)) {
		if(!$forcer) {
			return("D�p�che d�j� dans la base.");
		}
		else {
			$idUpdate = ($res[0]['idDepeche']);
		}
	}
	
	$tabfich=file($f); /* 
	$xml ='<?xml version="1.0" encoding="ISO-8859-1"?>';
	for($i=1;$i<count($tabfich);$i++) {
		$xml .= $tabfich[$i];
	} */
	
	try {
		if(!@$xml = simplexml_load_file($f)) {
			throw new Exception("Impossible de cr�er l'objet SimpleXMLElement");
		}
	} catch (Exception $e) {
		return utf8_encode($e->getMessage());
	}
					
	// $str .= "(" . $f . ")";
	$pathDepeche = $f;
	
	$HeadLine = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/NewsLines/HeadLine');
	// $str .= "\nHeadLine : ";
	// On boucle pour �tre s�r de ne pas en r�ter : normalement, une seule chose dans le HeadLine
	$titreDepeche = "";
	for($i=0;$i<count($HeadLine);++$i) {
		$titreDepeche .= $HeadLine[$i];
	}
	// $str .= $titreDepeche;
	
	$DateId = $xml->xpath('/Aggregate/NewsML/NewsItem/Identification/NewsIdentifier/DateId');
	// $str .= "\nDate : ";
	$dateDepeche = "";
	for($i=0;$i<count($DateId);++$i) {
		$dateDepeche .= $DateId[$i];
	}
	// $str .= $dateDepeche;
	
	$TopicSet = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/TopicSet/Topic/Description');
	// $str .= "\nTopic : ";
	$DataContent = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/ContentItem/DataContent/p');
	// $str .= "\nDataContent : ";
	
	$txtDepeche = "";
	$ten = array();
	$tquote = array();
	// count($DataContent)-1 pour ne pas afficher la signature : a v�rifier !
	for($i=0;$i<count($DataContent)-1;++$i) {
		$txtDepeche .= preg_replace('` {2,}`', ' ', utf8_decode($DataContent[$i]));
		
	}
	if(isset($idUpdate)) {
		$q = "
			UPDATE `$base`.depeche
			SET	`txtDepeche`= :dc,
				`titreDepeche` = :hl,
				`dateDepeche` = :dd,
				`pathDepeche` = :pth
			WHERE idDepeche = $idUpdate
			LIMIT 1
		";
	} else {
		$q = "
			INSERT INTO `$base`.depeche (`idDepeche`, `txtDepeche`, `titreDepeche`, `dateDepeche`, `pathDepeche`)
			VALUES (null,:dc,:hl,:dd,:pth);
		";
	}
	$temp = $dateDepeche;
	$dateDepeche = $temp[0] . $temp[1] . $temp[2] . $temp[3] . "-" . $temp[4] . $temp[5] . "-" . $temp [6] . $temp[7];
	$tmp = array(
		':dc' => array(($txtDepeche), PDO::PARAM_STR),
		':hl' => array(utf8_decode($titreDepeche), PDO::PARAM_STR),
		':dd' => array($dateDepeche, PDO::PARAM_STR),
		':pth' => array($pathDepeche, PDO::PARAM_STR)
	);
	$res = makePDOQuery($d,"INSERT", $q, $tmp);	
	
	$idDepeche = $d->lastInsertId();
	if(isset($idUpdate)) {
		$idDepeche = $idUpdate;
	}
	
	$tIPTC = array();
	for($i=0;$i<count($TopicSet);++$i) {
		// $str .= $TopicSet[$i] . "; ";
		array_push($tIPTC,$TopicSet[$i]);
	}
	// $str .= count($tIPTC);
	$q = "
		INSERT INTO `$base`.iptc (`idIPTC`, `lblIPTC`)
		VALUES (null,:li);
	";
	for($i=0;$i<count($tIPTC);++$i) {
		$tmp = array(
			':li' => array(ucfirst(utf8_decode($tIPTC[$i])), PDO::PARAM_STR)
		);
		$res = makePDOQuery($d,"INSERT", $q, $tmp);	
		$idIPTC = $d->lastInsertId();
		$qbis = "
			INSERT INTO `$base`.assocdepecheiptc (`idIPTC`, `idDepeche`)
			VALUES (:ii,:id);
		";
		$tmp = array(
			':ii' => array($idIPTC, PDO::PARAM_INT),
			':id' => array($idDepeche, PDO::PARAM_INT)
		);
		$res = makePDOQuery($d,"INSERT", $qbis, $tmp);	
	}
	
	$Verbatims = $xml->xpath('/Aggregate/verbatims/verbatim');
	foreach($Verbatims as $v) {
		$par = $v[0]['where'] . "";
		$par = split("_", $par);
		$par = $par[1];
		$start = $v[0]['start'] . "";
		$end = $v[0]['end'] . "";
		$txt = $v[0] . "";
		
		$q = "
			INSERT INTO `$base`.verbatim (`idVerbatim`,`parVerbatim`,`startVerbatim`,`endVerbatim`,`txtVerbatim`,`idDepeche`)
			VALUES(null, :pv, :sv, :ev, :tv, :id);
		";
	
		$ps = $d->prepare($q);
		$tmp = array(
			':pv' => array(($par), PDO::PARAM_STR),
			':sv' => array(($start), PDO::PARAM_STR),
			':ev' => array(($end), PDO::PARAM_STR),
			':tv' => array(utf8_decode($txt), PDO::PARAM_STR),
			':id' => array(($idDepeche), PDO::PARAM_STR)
		);
		PDOBindArray($ps,$tmp);
		$ps->execute();
	
	}
	
	$Entities = $xml->xpath('/Aggregate/entities/entity');
	$Token = $xml->xpath('/Aggregate/tokens/token');
	// $str .= "\nEntities : ";
	
	// count($DataContent)-1 pour ne pas afficher la signature : a v�rifier !
	// parcourt des entit�s
	$idToIdDB = array();
	for($i=0;$i<count($Entities)-1;++$i) {
		$name = utf8_decode($Entities[$i]['name'] . "");
		$type = utf8_decode($Entities[$i]['type'] . "");
		
		$q = "
			SELECT e.idEntite
			FROM `$base`.entite e, `$base`.typeentite te
			WHERE e.idTypeEntite_appartient_a = te.idTypeEntite
			AND e.lblEntite = :lbl
			AND te.lblTypeEntite = :lte
			LIMIT 0,1
		";
	
		$tmp = array(
			':lbl' => array(($name), PDO::PARAM_STR),
			':lte' => array(($type), PDO::PARAM_STR)
		);
		//echo $type;
		$res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();
		//echo "\n" . $name . "->" . count($res);
		
		if(count($res)) {
			$idEntite = $res[0][0];		
		}
		else {
			// $str .= $type . " - ";
			$q = "
				SELECT te.idTypeEntite
				FROM `$base`.typeentite te
				WHERE te.lblTypeEntite = :lbl
				LIMIT 0,1
			";
		
			$tmp = array(
				':lbl' => array(($type), PDO::PARAM_STR)
			);
			$res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();
			if(count($res)) {
				$idType = $res[0][0];		
			}
			else {
				$q = "
					INSERT INTO `$base`.typeentite (`idTypeEntite`, `lblTypeEntite`)
					VALUES (null,:lbl);
				";
			
				$tmp = array(
					':lbl' => array(($type), PDO::PARAM_STR)
				);
				$res = makePDOQuery($d,"SELECT", $q, $tmp);
				$idType = $d->lastInsertId();
			}
			
			$q = "
				INSERT INTO `$base`.entite (`idEntite`, `lblEntite`,`idTypeEntite_appartient_a`)
				VALUES (null,:lbl,:it);
			";
		
			$tmp = array(
				':lbl' => array(($name), PDO::PARAM_STR),
				':it' => array($idType, PDO::PARAM_INT)
			);
			$res = makePDOQuery($d,"INSERT", $q, $tmp);
			$idEntite = $d->lastInsertId();
		}
		$idToIdDB[$Entities[$i]['id'] . ""] = $idEntite . "";
		//print_r($idToIdDB);
		
		
		foreach($Entities[$i]->xpath('./occ') as $occ) {
			$q = "
				INSERT INTO `$base`.occurrence(`idEntite`,`idDepeche`,`lblOccurrence`,`parOccurrence`,`startOccurrence`,`endOccurrence`)
				VALUES (:ie,:id,:lo,:po,:so,:eo);
			";
			
			$ps = $d->prepare($q);
			$lblOccurrence = (String) $occ['alias'] . "";
			$parOccurrence = split("_",$occ['where']);
			$parOccurrence = $parOccurrence[1];
			$startOccurrence = (int) $occ['start'];
			$endOccurrence = (int) $occ['end'];
			$tmp = array(
				':ie' => array($idEntite, PDO::PARAM_INT),
				':id' => array($idDepeche, PDO::PARAM_INT),
				':lo' => array(utf8_decode($lblOccurrence), PDO::PARAM_STR),
				':po' => array($parOccurrence, PDO::PARAM_INT),
				':so' => array($startOccurrence, PDO::PARAM_INT),
				':eo' => array($endOccurrence, PDO::PARAM_INT)
			);
			PDOBindArray($ps,$tmp);
			$ps->execute();
		}
	}
	
			
		$Citations = $xml->xpath('/Aggregate/citations/citation');
		foreach($Citations as $c) {
			$pred = $c[0]['pred'];
			$agent = $c->xpath('./agent');
			$theme = $c->xpath('./theme');
			//print_r($agent);
		
			// Attention ! On n'entre dans le if que si l'on a trouver l'entit� correspondante � celle de la citation.
			// Donc pas d'entit� = pas d'enregistrement.
			
			// ne pas mettre idEntite_possede dans citation, mais faire une table d'association.
			$q = "
				INSERT INTO `$base`.citation (`idCitation`, `txtCitation`,`parCitation`,`startCitation`,`endCitation`,`predCitation`,`idDepeche_est_tiree_de`)
				VALUES (null,:txt,:par,:str,:end,:prd,:idd);
			";
			$ps = $d->prepare($q);
			//$theme_txt = "";
			$where = split("_",$theme[0]['where']);
			$tmp = array(
				':txt' => array(utf8_decode($theme[0]), PDO::PARAM_STR),
				':par' => array((String) $where[1],PDO::PARAM_INT),
				':str' => array((String) $theme[0]['start'],PDO::PARAM_INT),
				':end' => array((String) $theme[0]['end'],PDO::PARAM_INT),
				':prd' => array((String) $pred,PDO::PARAM_INT),
				':idd' => array($idDepeche, PDO::PARAM_INT)
			);
			PDOBindArray($ps,$tmp);
			$ps->execute();
			$idCitation = $d->lastInsertId();;
			
			
			if(isset($agent[0])) {
				$q = "
					INSERT INTO `$base`.agent (`idAgent` , `idEntite`, `idCitation`, `parAgent`, `startAgent`, `endAgent`, `txtAgent`)
					VALUES (null, :ide, :idc, :par, :str, :end, :txt);
				";
				$ps = $d->prepare($q);
				$where = split("_",$agent[0]['where']);
				$tmp = array(
					':ide' => array($idEntite, PDO::PARAM_INT),
					':idc' => array($idCitation, PDO::PARAM_INT),
					':par' => array((String) $where[1],PDO::PARAM_INT),
					':str' => array((String) $agent[0]['start'],PDO::PARAM_INT),
					':end' => array((String) $agent[0]['end'],PDO::PARAM_INT),
					':txt' => array(utf8_decode($agent[0]), PDO::PARAM_STR)
				);
				PDOBindArray($ps,$tmp);
				$ps->execute();
				
				if(isset($agent[0]['entity'])) {
					foreach($idToIdDB as $idXML => $idDB) {
						if($agent[0]['entity'] . "" == $idXML . "") {
							$q = "
								INSERT INTO `$base`.assoccitationentite (`idCitation`, `idEntite`)
								VALUES ($idCitation,$idDB);
							";
							//echo "\n" . $q;
							$ps = $d->prepare($q);
							$ps->execute();						
						}
					}
				}
			}
			
			if($audience = $c->xpath('./audience')) {
				$q = "
					INSERT INTO `$base`.audience (`idAudience`,`parAudience`,`startAudience`,`endAudience`,`entityAudience`,`isIndirectAudience`,`valueAudience`,`idCitation`)
					VALUES(null,:par,:str,:end,:ent,:isi,:val,:idc);
				";
				$ps = $d->prepare($q);
				//$theme_txt = "";
				$where = split("_",$audience[0]['where']);
				if(isset($audience[0]['indirect_entity'])) {
					$entity = split("_",$audience[0]['indirect_entity']);
					$entity = (String) $entity[1];
					$isIndirect = true;
				}
				elseif(isset($audience[0]['entity'])) {
					$entity = split("_",$audience[0]['entity']);
					$entity = (String) $entity[1];
					$isIndirect = false;
				}
				else {
					$entity = null;
					$isIndirect = null;
				}
				$tmp = array(
					':par' => array((int) $where[1],PDO::PARAM_INT),
					':str' => array((int) $audience[0]['start'],PDO::PARAM_INT),
					':end' => array((int) $audience[0]['end'],PDO::PARAM_INT),
					':ent' => array((int) $entity, PDO::PARAM_INT),
					':isi' => array((bool) $isIndirect, PDO::PARAM_BOOL),
					':val' => array((String) $audience[0], PDO::PARAM_STR),
					':idc' => array($idCitation, PDO::PARAM_INT)
				);
				//echo (String) $audience[0];
				PDOBindArray($ps,$tmp);
				$ps->execute();
			}
		}
	// $str .= "\n\n\n";
	
	//$d = null;
	// Fermer fichier
	return "Ok";
}
?>