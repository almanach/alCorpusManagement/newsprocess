<?php
  //ini_set("memory_limit","12M");
ini_set("memory_limit","120M");
set_time_limit(0);
function getmicrotime(){
    list($usec, $sec) = explode(" ",microtime());
    return ((float)$usec + (float)$sec);
}
$debut = getmicrotime();

// Retourne 1 si $d2 plus grand
function cmp_date($d1,$d2) {
	$td1 = array();
	$td2 = array();
	
	$td1 = explode(" ",$d1);
	$tmp = explode("-",$td1[0]);
	
	foreach($tmp as $date) {
		array_push($td1,$date);
	}
	$tmp = explode(":",$td1[1]);
	foreach($tmp as $date) {
		array_push($td1,$date);
	}
	$d1 = (int) $td1[2] . $td1[3] . $td1[4] . $td1[5] . $td1[6] . $td1[7];
	
	$td2 = explode(" ",$d2);
	$tmp = explode("-",$td2[0]);
	
	foreach($tmp as $date) {
		array_push($td2,$date);
	}
	$tmp = explode(":",$td2[1]);
	foreach($tmp as $date) {
		array_push($td2,$date);
	}
	$d2 = (int) $td2[2] . $td2[3] . $td2[4] . $td2[5] . $td2[6] . $td2[7];
	
	
	return ($d1 < $d2);
	
	// $td1 = $tmp + explode($td1[1]);
}

function putInDB($f,$d, $forcer = false) {
	$base = SQL_BASE;
	// $str = "";
	try {
		if(!@$xml = simplexml_load_file($f)) {
			throw new Exception("Impossible de créer l'objet SimpleXMLElement");
		}
	} catch (Exception $e) {
		return utf8_encode($e->getMessage());
	}
	$tpath = explode('-',$f);
	$idAFP = $tpath[count($tpath) - 3] . "-" . $tpath[count($tpath) - 2] . "-" .  substr($tpath[count($tpath) - 1],0,-4);

	$q = "
		SELECT d.idDepeche, d.creationTimeDepeche
		FROM `$base`.depeche d
		WHERE d.pathDepeche LIKE :afp
		LIMIT 0,1
	";
	$idAFP = "%{$idAFP}%";
	$tmp = array(
		':afp' => array($idAFP, PDO::PARAM_STR)
	);
	$res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();	
	
	
	$creationTime = $xml->xpath('/Aggregate');
	$ct = $creationTime[0]['creationTime'];
	//echo "\n" . $ct . " < " . $res[0]['creationTimeDepeche'] . " ? \n";
	if(count($res)) {
		if($forcer || cmp_date($res[0]['creationTimeDepeche'],$ct)) {
			$idD = $res[0]['creationTimeDepeche'];
			include_once("delete.inc.php");	
			echo "Nouvelle version : Mise � jour...";
			deleteDepecheById($res[0]['idDepeche']);
		}
		else {
			$xml = null;
			return("D�p�che d�j� � jour.");
		}
	}
	
	$tabfich=file($f); 
	$pathDepeche = $f;

        $NewsML = $xml->xpath('/Aggregate/NewsML');
        $version = $NewsML[0]['Version'];
        if ($version == "1.2") {
            $is12=1;
        } else {
            $is12=0;
        }

//        echo "Version is $version is12=$is12";
        if ($is12) {
            echo "Format 1.2";
        }
        
            
        
	$HeadLine = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/NewsLines/HeadLine');
	// $str .= "\nHeadLine : ";
	// On boucle pour être sûr de ne pas en râter : normalement, une seule chose dans le HeadLine
	
	$titreDepeche = "";
	for($i=0;$i<count($HeadLine);++$i) {
		$titreDepeche .= $HeadLine[$i];
	}
	// $str .= $titreDepeche;
	
	
	$DateId = $xml->xpath('/Aggregate/NewsML/NewsItem/Identification/NewsIdentifier/DateId');
	// $str .= "\nDate : ";
	$dateDepeche = "";
	for($i=0;$i<count($DateId);++$i) {
		$dateDepeche .= $DateId[$i];
	}
	// $str .= $dateDepeche;

        if ($is12) {
            $TopicSet = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/DescriptiveMetadata/Property[@FormalName="Keyword"]/@Value');
        } else {
            $TopicSet = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/TopicSet/Topic/Description');
        }
        
	// $str .= "\nTopic : ";
        if ($is12) {
            $DataContent = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/ContentItem/DataContent/nitf/body/body.content/p');
        } else {
            $DataContent = $xml->xpath('/Aggregate/NewsML/NewsItem/NewsComponent/ContentItem/DataContent/p');
        }
        
	// $str .= "\nDataContent : ";
	
	$txtDepeche = "";
	$ten = array();
	$tquote = array();
	// count($DataContent)-1 pour ne pas afficher la signature : a vérifier
	// !
        $bound = count($DataContent)-1;
            // if ($is12) {$bound = $bound - 1;}
	for($i=0;$i<$bound;++$i) {
            if ($is12) { $txtDepeche .= "\n"; }
            $txtDepeche .= preg_replace('` {2,}`', ' ', ($DataContent[$i]));
	}
	
	$q = "
		INSERT INTO `$base`.depeche (`idDepeche`, `txtDepeche`, `titreDepeche`, `dateDepeche`, `pathDepeche`, `creationTimeDepeche`)
		VALUES (null,:dc,:hl,:dd,:pth,'$ct');
	";
	$temp = $dateDepeche;
	$dateDepeche = $temp[0] . $temp[1] . $temp[2] . $temp[3] . "-" . $temp[4] . $temp[5] . "-" . $temp [6] . $temp[7];
	$tmp = array(
		':dc' => array(($txtDepeche), PDO::PARAM_STR),
		':hl' => array(($titreDepeche), PDO::PARAM_STR),
		':dd' => array($dateDepeche, PDO::PARAM_STR),
		':pth' => array($pathDepeche, PDO::PARAM_STR)
	);
	makePDOQuery($d,"INSERT", $q, $tmp);	
	
	$idDepeche = $d->lastInsertId();
	if(isset($idUpdate)) {
		$idDepeche = $idUpdate;
	}
	
	$tIPTC = array();
	for($i=0;$i<count($TopicSet);++$i) {
		// $str .= $TopicSet[$i] . "; ";
		array_push($tIPTC,$TopicSet[$i]);
	}
	// $str .= count($tIPTC);
	$q = "
		INSERT INTO `$base`.iptc (`idIPTC`, `lblIPTC`)
		VALUES (null,:li);
	";
	for($i=0;$i<count($tIPTC);++$i) {
		$tmp = array(
			':li' => array(ucfirst(($tIPTC[$i])), PDO::PARAM_STR)
		);
		makePDOQuery($d,"INSERT", $q, $tmp);	
		$idIPTC = $d->lastInsertId();
		$qbis = "
			INSERT INTO `$base`.assocdepecheiptc (`idIPTC`, `idDepeche`)
			VALUES (:ii,:id);
		";
		$tmp = array(
			':ii' => array($idIPTC, PDO::PARAM_INT),
			':id' => array($idDepeche, PDO::PARAM_INT)
		);
		makePDOQuery($d,"INSERT", $qbis, $tmp);	
	}
	
	$Verbatims = $xml->xpath('/Aggregate/verbatims/verbatim');
	foreach($Verbatims as $v) {
		$par = $v[0]['where'] . "";
//		$par = split("_", $par);
                $par = preg_split('/_/', $par);
		$par = $par[1];
		$start = $v[0]['start'] . "";
		$end = $v[0]['end'] . "";
		$txt = $v[0] . "";
		
		$q = "
			INSERT INTO `$base`.verbatim (`idVerbatim`,`parVerbatim`,`startVerbatim`,`endVerbatim`,`txtVerbatim`,`idDepeche`)
			VALUES(null, :pv, :sv, :ev, :tv, :id);
		";
	
		$ps = $d->prepare($q);
		$tmp = array(
			':pv' => array(($par), PDO::PARAM_STR),
			':sv' => array(($start), PDO::PARAM_STR),
			':ev' => array(($end), PDO::PARAM_STR),
			':tv' => array(($txt), PDO::PARAM_STR),
			':id' => array(($idDepeche), PDO::PARAM_STR)
		);
		PDOBindArray($ps,$tmp);
		$ps->execute();
	
	}
	
	$Entities = $xml->xpath('/Aggregate/entities/entity');
	$Token = $xml->xpath('/Aggregate/tokens/token');
	// $str .= "\nEntities : ";
	
	// count($DataContent)-1 pour ne pas afficher la signature : a vérifier !
	// parcourt des entités
	$idToIdDB = array();
	foreach($Entities as $entity) {
//            $entity = $Entities[$i];
            $name = ($entity['name'] . "");
            $type = ($entity['type'] . "");

            $name = preg_replace('/^\d+:/','',$name);
            
//            echo "\tEntity $name $type\n";
            
            $q = "
			SELECT e.idEntite, e.txtEntite, e.linkEntite
			FROM `$base`.entite e, `$base`.typeentite te
			WHERE e.idTypeEntite_appartient_a = te.idTypeEntite
			AND e.lblEntite = :lbl
			AND te.lblTypeEntite = :lte
			LIMIT 0,1
		";
            
            $tmp = array(
                ':lbl' => array(($name), PDO::PARAM_STR),
                ':lte' => array(($type), PDO::PARAM_STR)
                         );
		//echo $type;
            $res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();
		//echo "\n" . $name . "->" . count($res);
            
            
            $txtEntite = "";
            $href = "";
            $htype = "";
            if ($data = $entity->xpath('data')){
                $txtEntite = $data[0];
                if ($data[0]['href']) {
                    $href = $data[0]['href'];
                    $htype = $data[0]['htype'];
                }
            }
                
            if($txtEntite == "(UNKNOWN PERSON)" || $txtEntite == "(UNKNOWN PERSON f)" || $txtEntite == "(UNKNOWN PERSON m)" || $txtEntite == "(UNKNOWN LOCATION)" || $txtEntite == "(UNKNOWN ORGANIZATION)" || $txtEntite == "(UNKNOWN PRODUCT)") $txtEntite = "";
		
            if(count($res)) {
                $idEntite = $res[0]['idEntite'];
                if(strlen($res[0]['txtEntite']) < strlen($txtEntite)) {
                    $q = '
					UPDATE `'. $base . '`.entite
					SET txtEntite = "' . addslashes($txtEntite) . '"
					WHERE idEntite = ' . $idEntite . '
					LIMIT 1;
				';
                    makePDOQuery($d,"UPDATE", $q, array());
                }
                if(($res[0]['linkEntite'] == NULL) && isset($href)) {
                    $q = "
					UPDATE `$base`.entite
					SET linkEntite = '$href', typeLinkEntite = '$htype'
					WHERE idEntite = $idEntite
					LIMIT 1;
				";
                    makePDOQuery($d,"UPDATE", $q, array());
                }
            }
            else {
                    // $str .= $type . " - ";
                $q = "
				SELECT te.idTypeEntite
				FROM `$base`.typeentite te
				WHERE te.lblTypeEntite = :lbl
				LIMIT 0,1
			";
		
                $tmp = array(
                    ':lbl' => array(($type), PDO::PARAM_STR)
                             );
                $res = makePDOQuery($d,"SELECT", $q, $tmp)->fetchAll();
                if(count($res)) {
                    $idType = $res[0][0];
                }
                else {
                    $q = "
					INSERT INTO `$base`.typeentite (`idTypeEntite`, `lblTypeEntite`)
					VALUES (null,:lbl);
				";
                    
                    $tmp = array(
                        ':lbl' => array(($type), PDO::PARAM_STR)
                                 );
                    makePDOQuery($d,"SELECT", $q, $tmp);
                    $idType = $d->lastInsertId();
                }
                $q = "
				INSERT INTO `$base`.entite (`idEntite`, `lblEntite`,`idTypeEntite_appartient_a`,`txtEntite`, `linkEntite`,`typeLinkEntite`)
				VALUES (null,:lbl,:it,:txt,:lie,:tle);
			";
		
                $tmp = array(
                    ':lbl' => array(($name), PDO::PARAM_STR),
                    ':it' => array($idType, PDO::PARAM_INT),
                    ':txt' => array(($txtEntite), PDO::PARAM_STR),
                    ':lie' => array(($href), PDO::PARAM_STR),
                    ':tle' => array(($htype), PDO::PARAM_STR)
                             );
                makePDOQuery($d,"INSERT", $q, $tmp);
                $idEntite = $d->lastInsertId();
            }
            $idToIdDB[$entity['id'] . ""] = $idEntite . "";
		//print_r($idToIdDB);
		
		
            foreach($entity->xpath('occ') as $occ) {
                $q = "
				INSERT INTO `$base`.occurrence(`idEntite`,`idDepeche`,`lblOccurrence`,`parOccurrence`,`startOccurrence`,`endOccurrence`)
				VALUES (:ie,:id,:lo,:po,:so,:eo);
			";
                $ps = $d->prepare($q);
                $lblOccurrence = (String) $occ['alias'] . "";
//                $parOccurrence = split("_",$occ['where']);
                $parOccurrence = preg_split('/_/',$occ['where']);
                $parOccurrence = $parOccurrence[1];
                $startOccurrence = (int) $occ['start'];
                $endOccurrence = (int) $occ['end'];
//                echo "\t\tOccurrence $lblOccurrence\n";
                $tmp = array(
                    ':ie' => array($idEntite, PDO::PARAM_INT),
                    ':id' => array($idDepeche, PDO::PARAM_INT),
                    ':lo' => array(($lblOccurrence), PDO::PARAM_STR),
                    ':po' => array($parOccurrence, PDO::PARAM_INT),
                    ':so' => array($startOccurrence, PDO::PARAM_INT),
                    ':eo' => array($endOccurrence, PDO::PARAM_INT)
                             );
                PDOBindArray($ps,$tmp);
                $ps->execute();
            }
	}
	
			
	$Citations = $xml->xpath('/Aggregate/citations/citation');
	foreach($Citations as $c) {
		$agent = $c->xpath('./agent');
		$theme = $c->xpath('./theme');
		//print_r($agent);
	
		// Attention ! On n'entre dans le if que si l'on a trouver l'entité correspondante à celle de la citation.
		// Donc pas d'entité = pas d'enregistrement.
		
		// ne pas mettre idEntite_possede dans citation, mais faire une table d'association.
		$q = "
			INSERT INTO `$base`.citation (`idCitation`, `txtCitation`,`parCitation`,`startCitation`,`endCitation`,`idDepeche_est_tiree_de`)
			VALUES (null,:txt,:par,:str,:end,:idd);
		";
		$ps = $d->prepare($q);
		//$theme_txt = "";
//		$where = split("_",$theme[0]['where']);
                $where = preg_split('/_/',$theme[0]['where']);
		$tmp = array(
			':txt' => array(($theme[0]), PDO::PARAM_STR),
			':par' => array((String) $where[1],PDO::PARAM_INT),
			':str' => array((String) $theme[0]['start'],PDO::PARAM_INT),
			':end' => array((String) $theme[0]['end'],PDO::PARAM_INT),
			':idd' => array($idDepeche, PDO::PARAM_INT)
		);
		PDOBindArray($ps,$tmp);
		$ps->execute();
		$idCitation = $d->lastInsertId();;

                if (isset($c['where'])) {
                    $q = "
			INSERT INTO `$base`.predicat (`idPredicat`,`idCitation`, `parPredicat`, `startPredicat`, `endPredicat`)
			VALUES (null, :idc, :par, :str, :end );
			";
                    $ps = $d->prepare($q);
//                    $where = split("_",$c['where']);
                    $where = preg_split('/_/',$c['where']);
                    $tmp = array(
                        ':idc' => array($idCitation, PDO::PARAM_INT),
                        ':par' => array((String) $where[1],PDO::PARAM_INT),
                        ':str' => array((String) $c['start'],PDO::PARAM_INT),
                        ':end' => array((String) $c['end'],PDO::PARAM_INT),
                                 );
                    PDOBindArray($ps,$tmp);
                    $ps->execute();
                }
		
		if(isset($agent[0])) {
			$q = "
				INSERT INTO `$base`.agent (`idAgent` , `idEntite`, `idCitation`, `parAgent`, `startAgent`, `endAgent`, `txtAgent`)
				VALUES (null, :ide, :idc, :par, :str, :end, :txt);
			";
			$ps = $d->prepare($q);
//			$where = split("_",$agent[0]['where']);
                        $where = preg_split('/_/',$agent[0]['where']);
			if($idEntite) $idE = 0;
			else $idE = $idEntite;
			
			$tmp = array(
				':ide' => array($idE, PDO::PARAM_INT),
				':idc' => array($idCitation, PDO::PARAM_INT),
				':par' => array((String) $where[1],PDO::PARAM_INT),
				':str' => array((String) $agent[0]['start'],PDO::PARAM_INT),
				':end' => array((String) $agent[0]['end'],PDO::PARAM_INT),
				':txt' => array(($agent[0]), PDO::PARAM_STR)
			);
			PDOBindArray($ps,$tmp);
			$ps->execute();
			
			if(isset($agent[0]['entity'])) {
				foreach($idToIdDB as $idXML => $idDB) {
					if($agent[0]['entity'] . "" == $idXML . "") {
						$q = "
							INSERT INTO `$base`.assoccitationentite (`idCitation`, `idEntite`)
							VALUES ($idCitation,$idDB);
						";
						//echo "\n" . $q;
						$ps = $d->prepare($q);
						$ps->execute();						
					}
				}
			}
		}
		
		if($audience = $c->xpath('./audience')) {
			$q = "
				INSERT INTO `$base`.audience (`idAudience`,`parAudience`,`startAudience`,`endAudience`,`entityAudience`,`isIndirectAudience`,`valueAudience`,`idCitation`)
				VALUES(null,:par,:str,:end,:ent,:isi,:val,:idc);
			";
			$ps = $d->prepare($q);
			//$theme_txt = "";
//			$where = split("_",$audience[0]['where']);
                        $where = preg_split('/_/',$audience[0]['where']);
			if(isset($audience[0]['indirect_entity'])) {
//				$entity =
//				split("_",$audience[0]['indirect_entity']);
                            $entity = preg_split('/_/',$audience[0]['indirect_entity']);
				$entity = (String) $entity[1];
				$isIndirect = true;
			}
			elseif(isset($audience[0]['entity'])) {
//				$entity = split("_",$audience[0]['entity']);
                            $entity = preg_split('/_/',$audience[0]['entity']);
				$entity = (String) $entity[1];
				$isIndirect = false;
			}
			else {
				$entity = null;
				$isIndirect = null;
			}
			$tmp = array(
				':par' => array((int) $where[1],PDO::PARAM_INT),
				':str' => array((int) $audience[0]['start'],PDO::PARAM_INT),
				':end' => array((int) $audience[0]['end'],PDO::PARAM_INT),
				':ent' => array((int) $entity, PDO::PARAM_INT),
				':isi' => array((bool) $isIndirect, PDO::PARAM_BOOL),
				':val' => array((String) $audience[0], PDO::PARAM_STR),
				':idc' => array($idCitation, PDO::PARAM_INT)
			);
			//echo (String) $audience[0];
			PDOBindArray($ps,$tmp);
			$ps->execute();
		}

                    // Looading dates
                if($date = $c->xpath('./date')) {
                    $q = "
				INSERT INTO `$base`.date (`idDate`,`parDate`,`startDate`,`endDate`,`entityDate`,`isIndirectDate`,`valueDate`,`idCitation`)
				VALUES(null,:par,:str,:end,:ent,:isi,:val,:idc);
			";
                    $ps = $d->prepare($q);
			//$theme_txt = "";
//                    $where = split("_",$date[0]['where']);
                    $where = preg_split('/_/',$date[0]['where']);
                    if(isset($date[0]['indirect_entity'])) {
//                        $entity = split("_",$date[0]['indirect_entity']);
                        $entity = preg_split('/_/',$date[0]['indirect_entity']);
                        $entity = (String) $entity[1];
                        $isIndirect = true;
                    }
                    elseif(isset($date[0]['entity'])) {
//                        $entity = split("_",$date[0]['entity']);
                        $entity = preg_split('/_/',$date[0]['entity']);
                        $entity = (String) $entity[1];
                        $isIndirect = false;
                    }
                    else {
                        $entity = null;
                        $isIndirect = null;
                    }
                    $tmp = array(
                        ':par' => array((int) $where[1],PDO::PARAM_INT),
                        ':str' => array((int) $date[0]['start'],PDO::PARAM_INT),
                        ':end' => array((int) $date[0]['end'],PDO::PARAM_INT),
                        ':ent' => array((int) $entity, PDO::PARAM_INT),
                        ':isi' => array((bool) $isIndirect, PDO::PARAM_BOOL),
                        ':val' => array((String) $date[0], PDO::PARAM_STR),
                        ':idc' => array($idCitation, PDO::PARAM_INT)
                                 );
			//echo (String) $date[0];
                    PDOBindArray($ps,$tmp);
                    $ps->execute();
                }

                    // all loaded
        }
        
	// $str .= "\n\n\n";
	$xml = null;
	//$d = null;
	// Fermer fichier
	
	return "Ok";
}



echo "<pre>";
include_once('./common/sql/query.php');
$i=1;


function put($rep,$connex,$forcer = false) {
	global $i,$debut;
	
	if(!@opendir($rep)) exit("Chemin invalide");
	$dir = opendir($rep);
	while ($f = readdir($dir)) {
		if(is_file($rep.$f)) {
			echo $i . "°/ " . $f . "\t -> ";
			echo putInDB($rep.$f,$connex,$forcer);
			echo " (". (getmicrotime() - $debut) .")\n";
			++$i;
		}
		else{
			if($f != "." && $f != "..") {
				put($rep . $f . "/",$connex,$forcer);
			}
		}
	}
	closedir($dir);
}

$connex = connex();
// use true to force reload
// change directory to load news strored in that directeory
// put("AFP/",$connex, false);
put("results.retraites/",$connex,true);
$q = "OPTIMIZE TABLE `agent`, `assoccitationentite`, `assocdepecheiptc`, `audience`, `citation`, `depeche`, `entite`, `iptc`, `occurrence`, `predicat`, `typeentite`, `verbatim`";
$ps = $connex->prepare($q);
$ps->execute();

$connex = null; 
$fin = getmicrotime();
echo "Temps :  ".round($fin-$debut, 8) ." secondes.<br /> (" . round(($fin-$debut)/$i, 8) . " en moyenne)";
echo "</pre>";

?>
