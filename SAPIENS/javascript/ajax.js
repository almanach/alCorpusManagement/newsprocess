/*
	Fonctions AJAX
*/
function connex() {
	var xhr = false; 
	try {  xhr = new ActiveXObject('Msxml2.XMLHTTP');   }
	catch (e) {
		try {   xhr = new ActiveXObject('Microsoft.XMLHTTP');    }
		catch (e2) {
			try {  xhr = new XMLHttpRequest();     }
			catch (e3) {  xhr = false;   }
		}
	}
	return xhr;
}
function generate(w) {
	if(w) var what = w;
	else var what = false;
	var im = new Image();
	var xhr = connex();
	
	xhr.onreadystatechange  = function() {
		switch(xhr.readyState) {
			case 0 :
			case 1 :
			case 2 :
			case 3 :
				if(im.src) break;
				im.src = "./"+VAR_PATH_TEMPLATES+"images/ajax-loader.gif";
				document.getElementById('tagcloud').innerHTML = "";
				document.getElementById('tagcloud').appendChild(im);
				break;				
			case 4 :
				if(xhr.status  == 200)  {
					document.getElementById('tagcloud').innerHTML = xhr.responseText;
				}
				else {
					alert("Error code " + xhr.status);
				}
				break;
			default:
				break;
		
		}
    };
	if(what == "slugs") xhr.open("POST", "./slugscloud.php", true);
	else xhr.open("POST", "./tagcloud.php", true);
	xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	xhr.send("");

}
