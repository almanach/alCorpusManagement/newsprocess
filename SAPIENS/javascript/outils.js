function calcBgById(idE) {
	var graine = (idE.substring(1,4));
	while(graine[0] == "0") {
		graine = graine.substring(1,graine.length);
	}
	graine = parseInt(graine);
	var r,g,b;
	r = g = b = graine+1;
	switch(graine % 10) {
		case 0:
			do {
				b *= graine+7;
			}while((b + r)%255 < 125)
			r = b;
			break;
		case 1:
			do {
				b *= graine+7;
			}while((b)%255 < 70)
			g = b;
			break;
		case 2:
			do {
				b *= graine+7;
			}while(b%255 < 125)
			break;
		case 3:
			do {
				g *= graine+45;
			}while(g%255 < 125)
			do {			
				r +=100;
			}while(r%255 <100)
			break;
		case 4:
			do {
				b *= graine+45;
			}while(b < 125)
			g = r = parseInt(b/2);
			break;
		case 5:
			do {
				g *= graine+45;
			}while(g < 100)
			b = r = parseInt(g/2);
			break;
		case 6:
			r = g = 255 - (graine*4)%100;
			b -=  b/10;
			b = Math.floor(b);
			break;
		case 7:
			g = g*10;
		case 8:
			b *= graine * 5 
		case 9:	
			r *= graine;
			b = r;
			while(r%255 < 140) {
				r *= graine;
			}
			break;
	}
	return "rgb("+r%255+","+g%255+","+b%255+")";
}

function getElementsByClassName(className, tag, elm){
	var testClass = new RegExp("(^|s)" + className + "(s|$)");
	var tag = tag || "*";
	var elm = elm || document;
	var elements = (tag == "*" && elm.all)? elm.all : elm.getElementsByTagName(tag);
	var returnElements = [];
	var current;
	var length = elements.length;
	for(var i=0; i<length; i++){
		current = elements[i ];
		if(testClass.test(current.className)){
			returnElements.push(current);
		}
	}
	return returnElements;
}

function emptyNode(n) {
	if(!n) return true;
	while(n.firstChild) {
		n.removeChild(n.firstChild);
	}
	return true;
}
// Récupérer le texte selectionné : tout navigateur.
/*
function txtsel() {
	if(window.selection) return "1" + window.selection();
	if (document.getSelection) return "2" + document.getSelection;
	if(window.getSelection) return "3" + window.getSelection();
	if (document.selection) return "4" + document.selection.createRange().text;
	return false;
}*/
