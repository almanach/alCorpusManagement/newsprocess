<?php
/*
Notes de fonctionnement :
	PHP5
Notes de maintenance :
	Les constantes se trouvent dans common/vars.php
Contacts :
	Eric.De_La_Clergerie@inria.fr
	vmignot@gmail.com
*/
header('Content-Type: text/html; charset=UTF-8');
ini_set("mbstring.internal_encoding","UTF-8");

function my_decode ($str) {
  return $str;
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<?php
	include('common/vars.php');
?>
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en">
<?php
	include('common/pages/head.php');
?>
<body>
<?php
	include('common/pages/body.php');
?>
</body>
</html>